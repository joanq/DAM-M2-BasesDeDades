= Activitat: Consultes d'agregació
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

== Base de dades

Per aquests exercicis utilitzarem la base de dades
link:https://github.com/lerocha/chinook-database[Chinook], que es pot utilitzar
en diversos SGBD.

La base de dades _Chinook_ modelitza una botiga de música digital. Inclou
taules per àlbums, artistes, pistes (`Tracks`), clients, empleats i factures.

Per importar-la al MySQL/MariaDB cal descarregar el fitxer
link:https://github.com/lerocha/chinook-database/raw/master/ChinookDatabase/DataSources/Chinook_MySql_AutoIncrementPKs.sql[Chinook_MySQL_AutoIncrementPKs.sql]
i importar-lo.

== Consultes

1. Consulta quants àlbums hi ha a la base de dades.
+
[source,sql]
----
SELECT COUNT(*)
 FROM Album;
----

2. Cerca quants grups (`Artist`) hi ha el nom dels quals comenci per l'article
_The_.
+
[source,sql]
----
SELECT COUNT(*)
 FROM Artist
 WHERE Name LIKE 'The %';
----

3. Amb dues consultes, cerca quants àlbums hi ha de _Deep Purple_.
+
[source,sql]
----
SELECT ArtistId
 FROM Artist
 WHERE Name LIKE 'Deep Purple';

SELECT COUNT(*)
 FROM Album
 WHERE ArtistId=58;
----

4. Amb dues consultes, cerca els tres grups dels quals hi ha més àlbums.
+
[source,sql]
----
SELECT ArtistId
 FROM Album
 GROUP BY ArtistId
 ORDER BY COUNT(*) DESC
 LIMIT 3;

SELECT Name FROM Artist
 WHERE ArtistId IN (90,22,58);
----

5. Cerca la durada mitjana de totes les pistes. Volem el resultat en segons.
+
[source,sql]
----
SELECT AVG(Milliseconds)/1000
 FROM Track;
----

6. Amb tres consultes, cerca els 5 àlbums que tenen una durada mitjana de les
seves pistes més gran. De cada àlbum en volem el seu nom i el nom del grup.
+
[source,sql]
----
SELECT AlbumId
 FROM Track
 GROUP BY AlbumId
 ORDER BY AVG(Milliseconds) DESC
 LIMIT 5;

SELECT Title, ArtistId
 FROM Album
 WHERE AlbumId IN (253,227,229,231,226);

SELECT Name
 FROM Artist
 WHERE ArtistId IN (147,149,158);
----

7. Amb dues consultes, cerca el nom dels tres gèneres dels quals en tenim més
pistes.
+
[source,sql]
----
SELECT GenreId
 FROM Track
 GROUP BY GenreId
 ORDER BY COUNT(*) DESC
 LIMIT 3;

SELECT Name
 FROM Genre
 WHERE GenreId IN (1,7,3);
----

8. Cerca quantes pistes hi ha en què hi consti algun compositor.
+
[source,sql]
----
SELECT COUNT(*)
 FROM Track
 WHERE Composer IS NOT NULL;
----

9. Cerca quants minuts de música disposem del compositor _Johann Sebastian Bach_.
+
[source,sql]
----
SELECT SUM(Milliseconds)/60000
 FROM Track
 WHERE Composer LIKE 'Johann Sebastian Bach';
----

10. Cerca el preu mitjà per pista. En una segona consulta, els diversos preus
que hi ha, i quantes pistes hi ha de cadascun d'ells.
+
[source,sql]
----
SELECT AVG(UnitPrice)
 FROM Track;

SELECT UnitPrice, COUNT(*)
 FROM Track
 GROUP BY UnitPrice;
----

11. Selecciona els MB que ocupa cadascun dels àlbums. Ordena els resultats
de més a menys MB. Per cada àlbum mostra el seu identificador i els MB que
ocupa.
+
[source,sql]
----
SELECT AlbumId, SUM(Bytes)/1024/1024 AS MB
 FROM Track
 GROUP BY AlbumId
 ORDER BY MB DESC;
----

12. Amb dues consultes, obté el nom de les llistes (`Playlist`) que contenen
més de mil cançons.
+
[source,sql]
----
SELECT PlaylistId
 FROM PlaylistTrack
 GROUP BY PlaylistId
 HAVING COUNT(*) > 1000;

SELECT Name
 FROM Playlist
 WHERE PlaylistId IN (1,5,8);
----

13. Descobreix quants clients tenen alguna dada introduïda a `State`. Fes-ho de
dues maneres diferents.
+
[source,sql]
----
SELECT COUNT(State)
 FROM Customer;

SELECT COUNT(*)
 FROM Customer
 WHERE State IS NOT NULL;
----

14. Descobreix quants clients tenim de cadascuna de les ciutats de Brasil.
+
[source,sql]
----
SELECT City, COUNT(*)
 FROM Customer
 WHERE Country LIKE 'Brazil'
 GROUP BY City;
----

15. Descobreix de quines ciutats d'Estats Units i de França tenim més d'un
client. Volem saber-ne el nom, si són d'un país o de l'altre, i quants clients
hi ha.
+
[source,sql]
----
SELECT Country, City, COUNT(*) AS NumCustomers
 FROM Customer
 WHERE Country LIKE 'France'
  OR Country LIKE 'USA'
 GROUP BY City, Country
 HAVING NumCustomers>1;
----

16. Amb dues consultes, troba quins països tenen exactament la mateixa
quantitat de factures (`Invoice`) que el país que en té menys.
+
[source,sql]
----
SELECT COUNT(*) AS NumInvoices
 FROM Invoice
 GROUP BY BillingCountry
 ORDER BY NumInvoices
 LIMIT 1;

SELECT BillingCountry
 FROM Invoice
 GROUP BY BillingCountry
 HAVING COUNT(*)=7;
----

17. Utilitzant només la taula `InvoiceLine`, calcula el preu total de la
factura número 4. En una segona consulta, comprova que la informació coincideix
amb la que hi ha a la taula `Invoice`.
+
[source,sql]
----
SELECT SUM(UnitPrice*Quantity)
 FROM InvoiceLine
 WHERE InvoiceId=4;

SELECT Total
 FROM Invoice
 WHERE InvoiceId=4;
----

18. Utilitzant només la taula `InvoiceLine`, cerca quines factures tenen un
preu total superior o igual a 20. Per a cadascuna mostra l'identificador de la
factura i el preu total.
+
[source,sql]
----
SELECT InvoiceId, SUM(UnitPrice*Quantity) AS Total
 FROM InvoiceLine
 GROUP BY InvoiceId
 HAVING Total >= 20;
----

19. Cerca els identificadors de les cançons que ens han reportat més de 2 dòlars
de beneficis.
+
[source,sql]
----
SELECT TrackId, SUM(UnitPrice*Quantity) AS Total
 FROM InvoiceLine
 GROUP BY TrackId
 HAVING Total>=2;
----

20. Cerca des de quins països es fan comandes que, de mitjana, superen els 6
dòlars de preu. Mostra el nom dels països i la mitjana de les comandes que li
corresponen.
+
[source,sql]
----
SELECT BillingCountry, AVG(Total) AS Average
 FROM Invoice
 GROUP BY BillingCountry
 HAVING Average>6;
----

21. Sabent que la funció `YEAR` ens permet seleccionar l'any d'una data, troba
quants diners hem cobrat cada any. Ordena els resultats de més guanys a menys.
+
[source,sql]
----
SELECT YEAR(InvoiceDate) AS Year, SUM(Total) AS Sum
 FROM Invoice
 GROUP BY Year
 ORDER BY Sum DESC;
----

22. Sabent que la funció `MONTH` ens permet seleccionar el mes d'una data,
utilitza dues consultes per trobar per quins mesos la mitjana del preu de les
factures supera la mitjana global. Ordena els resultats de mitjanes més gran a
mitjanes més petites.
+
[source,sql]
----
SELECT AVG(Total)
 FROM Invoice;

SELECT MONTH(InvoiceDate) AS Month, AVG(Total) AS Average
 FROM Invoice
 GROUP BY Month
 HAVING Average>5.651942
 ORDER BY Average DESC;
----
