= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions SQL

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

=== Base de dades `world`

5. Crea una funció d'agregació que retorni la cadena més llarga d'un conjunt.
Utilitza-la per trobar quin país té el nom més llarg.
