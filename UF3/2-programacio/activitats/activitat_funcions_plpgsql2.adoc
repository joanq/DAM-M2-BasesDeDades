= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions PL/pgSQL sobre pagila

Utilitzarem la base de dades `pagila`.

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

1. Fes la funció `search_film` que rebi com a paràmetre un text i retorni un
conjunt de pel·lícules. La funció comprovarà si l'argument conté algun símbol %.
Si no en conté n'afegirà un al principi i un al final. Llavors, cercarà totes
les pel·lícules el títol de les quals coincideixi amb el text rebut.

2. Fes una versió millorada de la funció anterior, `search_film`, que rebi
dos textos. El segon argument podrà ser "title", "description" o "genre". La
cerca es farà com a l'exercici anterior, però pel camp especificat. Si el
camp és incorrecte, cal llançar una excepció.

3. Fes una funció que rebi el títol d'una pel·lícula i un codi de botiga i
retorni els ítems d'inventari corresponents a aquesta pel·lícula que estiguin a
la botiga indicada i que, a més, no estiguin en lloguer en aquests moments.
+
[TIP]
====
Ja hi ha una funció, `inventory_in_stock`, que retorna si un ítem
d'inventari està en estoc o no.
====

4. Crea una funció que mostri el nom complet, el telèfon i el títol de la
pel·lícula de tots aquells clients que tenen un títol que ja haurien d'haver
retornat. La funció retornarà un conjunt amb tots aquests clients.

5. Fes una funció que permeti retornar una pel·lícula. La funció rebrà
l'identificador del client i l'identificador de l'ítem a retornar. Cal que es
llanci una excepció en cas que el client no tingui en lloguer l'ítem
especificat. El text de l'excepció ha d'indicar si l'ítem no està llogat o, si
ho està, ha d'indicar quin client el té en lloguer.
