= Llenguatges SQL: DCL
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat creació usuaris

Per fer aquesta feina necessites connectar-te amb l'usuari `postgres` que té
permisos d'administració.

Necessitaràs la BD `hotel` i la BD `miniwind`. No facis servir assistent gràfic
si no es demana.

Entrega un fitxer .sql amb les solucions.

1. Crea l'usuari `terrassa` sense contrasenya i no li assignis cap privilegi.
Prova de fer la connexió a la BD `hotel` des del _DBeaver_ amb aquest usuari.
Pots?
+
[source,sql]
----
vagrant@postgresqlserver:~$ sudo su - postgres
postgres@postgresqlserver:~$ createuser terrassa
----
+
====
No es pot fer la connexió remotament perquè l'usuari no té una contrasenya
assignada.
====

2. Crea l'usuari `sabadell` amb contrasenya `super3` i ara prova de fer la
connexió a la BD `hotel` amb el _DBeaver_. Pots? Justifica la resposta.
+
[source,sql]
----
postgres@postgresqlserver:~$ createuser -P sabadell
Enter password for new role:
Enter it again:
----
+
====
Podem establir connexió perquè l'usuari `sabadell` té contrasenya i per defecte
s'assigna el permís de connexió a qualsevol base de dades a tots els rols.
====

3. Prova d'obrir la taula `customers` de la BD `hotel` des del _DBeaver_ amb
aquesta connexió. Et deixa? Justifica la resposta.
+
====
No et deixa, perquè l'usuari `sabadell` no té permís de _USAGE_ sobre l'esquema
`hotel`, ni permís de _SELECT_ sobre la taula `customers`.
====

4. Quins privilegis tenen per defecte aquests usuaris que has creat abans?
Consulta els apunts.
+
====
Connexió sobre qualsevol base de dades, creació de taules temporals, execució
de funcions, ús de llenguatges de programació, i ús i creació sobre l'esquema
`public` de totes les bases de dades.
====

5. Ara, des de la línia d'ordres, llista els usuaris que hi ha i mira els
privilegis que tenen cadascun d'ells.
+
[source,sql]
----
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 admin     | Create role, Create DB                                     | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 sabadell  |                                                            | {}
 terrassa  |                                                            | {}
----

6. Prova de connectar-te amb la comanda `psql -h 127.0.0.1 -U terrassa -d hotel`.
Explica cada opció.
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U terrassa -d hotel
Password for user terrassa:
psql: fe_sendauth: no password supplied
----
+
====
-h indica la IP o nom de host on ens volem connectar.
-U indica l'usuari que utilitzarem per establir la connexió.
-d indica la base de dades on ens connectarem inicialment.
====

7. Connectat ara a la BD `hotel` amb l'usuari `admin`.
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U admin -d hotel
Password for user admin:
psql (11.7 (Debian 11.7-0+deb10u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

hotel=>
----

8. Concedeix a l'usuari `sabadell` el privilegi de consultar la taula `customers`
i prova que funciona fent alguna consulta.
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U admin -d hotel
Password for user admin:
psql (11.7 (Debian 11.7-0+deb10u1))
Type "help" for help.

hotel=# grant select on hotel.customers to sabadell;
GRANT
----
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U sabadell -d hotel
Password for user sabadell:
psql (11.7 (Debian 11.7-0+deb10u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.
                      ^
hotel=> select * from hotel.customers;
ERROR:  permission denied for schema hotel
LINE 1: select * from hotel.customers;
                      ^
hotel=>
----
+
====
Per poder fer la consulta sobre `customers` necessitem, a més de `SELECT` sobre
`customers`, `USAGE` sobre l'esquema `hotel`.
====
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U admin -d hotel
Password for user admin:
psql (11.7 (Debian 11.7-0+deb10u1))
Type "help" for help.

hotel=# grant usage on schema hotel to sabadell;
GRANT
hotel=#
----
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U sabadell -d hotel
Password for user sabadell:
psql (11.7 (Debian 11.7-0+deb10u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

hotel=> select * from hotel.customers limit 1;
 id |         email          | firstname | lastname | nationality | phonenumber
----+------------------------+-----------+----------+-------------+--------------
  1 | william_white@noaa.net | WILLIAM   | WHITE    | English     | +44795973396
(1 row)

hotel=>
----

9. Consulta ara els privilegis de cada taula de la BD `hotel`.
+
[source,sql]
----
hotel=# \dp
                                         Access privileges
 Schema |           Name            |   Type   |  Access privileges  | Column privileges | Policies
--------+---------------------------+----------+---------------------+-------------------+----------
 hotel  | bookingcalendar           | table    |                     |                   |
 hotel  | bookings                  | table    |                     |                   |
 hotel  | bookings_id_seq           | sequence |                     |                   |
 hotel  | customers                 | table    | admin=arwdDxt/admin+|                   |
        |                           |          | sabadell=r/admin    |                   |
 hotel  | customers_id_seq          | sequence |                     |                   |
 hotel  | facilities                | table    |                     |                   |
 hotel  | facilities_id_seq         | sequence |                     |                   |
 hotel  | hosts                     | table    |                     |                   |
 hotel  | hosts_id_seq              | sequence |                     |                   |
 hotel  | priceseasons              | table    |                     |                   |
 hotel  | priceseasons_id_seq       | sequence |                     |                   |
 hotel  | rooms                     | table    |                     |                   |
 hotel  | rooms_roomnumber_seq      | sequence |                     |                   |
 hotel  | roomtypefacilities        | table    |                     |                   |
 hotel  | roomtypefacilities_id_seq | sequence |                     |                   |
 hotel  | roomtypes                 | table    |                     |                   |
 hotel  | roomtypes_id_seq          | sequence |                     |                   |
 hotel  | seasons                   | table    |                     |                   |
 hotel  | seasons_id_seq            | sequence |                     |                   |
 hotel  | stayhosts                 | table    |                     |                   |
 hotel  | stays                     | table    |                     |                   |
 hotel  | stays_id_seq              | sequence |                     |                   |
(22 rows)
----

10. Explica els privilegis de la taula `customers`.
+
====
admin=arwdDxt/admin+: el rol `admin` té tots els privilegis i se'ls ha atorgat
ell mateix (és a dir, és el propietari).
sabadell=r/admin: el rol `sabadell` té el privilegi de lectura (_SELECT_) i li
ha atorgat el rol `admin`.
====

11. Concedeix a l'usuari `sabadell` el privilegi de fer _UPDATE_ i _INSERT_ a la
taula `bookings`. Comprova que funciona fent alguna inserció.
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -U admin -d hotel -h 127.0.0.1
Password for user admin:
psql (11.7 (Debian 11.7-0+deb10u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

hotel=> grant update, insert on hotel.bookings to sabadell;
GRANT
hotel=> grant usage on sequence bookings_id_seq to sabadell;
GRANT
----
+
[source,sql]
----
postgres@postgresqlserver:~$ psql -h 127.0.0.1 -U sabadell -d hotel
Password for user sabadell:
psql (11.7 (Debian 11.7-0+deb10u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

hotel=> insert into hotel.bookings(reservationdatetime, customerid, roomtypeid, checkin, checkout, price, state, nhosts) values (now(), 1, 1, '2020-03-15', '2020-03-18', 0, 'Reserved', 2);
INSERT 0 1
----

12. Consulta de nou els privilegis de cada taula.
+
[source,sql]
----
hotel=> \dp
                                         Access privileges
 Schema |           Name            |   Type   |  Access privileges  | Column privileges | Policies
--------+---------------------------+----------+---------------------+-------------------+----------
 hotel  | bookingcalendar           | table    |                     |                   |
 hotel  | bookings                  | table    | admin=arwdDxt/admin+|                   |
        |                           |          | sabadell=aw/admin   |                   |
 hotel  | bookings_id_seq           | sequence | admin=rwU/admin    +|                   |
        |                           |          | sabadell=U/admin    |                   |
 hotel  | customers                 | table    | admin=arwdDxt/admin+|                   |
        |                           |          | sabadell=r/admin    |                   |
 hotel  | customers_id_seq          | sequence |                     |                   |
 ...
----

13. Dóna el privilegi de poder consultar totes les taules de la BD `hotel` a
l’usuari `sabadell`. Comprova que funciona fent alguna consulta contra alguna
taula.
+
[source,sql]
----
hotel=> grant select on all tables in schema hotel to sabadell;
GRANT
----
+
[source,sql]
----
hotel=> select * from rooms limit 1;
 roomnumber | roomtypeid | empty
------------+------------+-------
          1 |          1 | t
(1 row)
----

14. Ara prova de donar-li el privilegi de consultar la taula `products` de la BD
`miniwind` a l'usuari `sabadell`. Comprova els privilegis.
+
[source,sql]
----
miniwind=> grant select on products to sabadell;
GRANT
miniwind=> grant usage on schema miniwind to sabadell;
GRANT
miniwind=> \dp
                                        Access privileges
  Schema  |         Name         |   Type   |  Access privileges  | Column privileges | Policies
----------+----------------------+----------+---------------------+-------------------+----------
 miniwind | customers            | table    |                     |                   |
 miniwind | customers_id_seq     | sequence |                     |                   |
 miniwind | employees            | table    |                     |                   |
 miniwind | employees_id_seq     | sequence |                     |                   |
 miniwind | invoices             | table    |                     |                   |
 miniwind | invoices_id_seq      | sequence |                     |                   |
 miniwind | order_details        | table    |                     |                   |
 miniwind | order_details_id_seq | sequence |                     |                   |
 miniwind | order_details_status | table    |                     |                   |
 miniwind | orders               | table    |                     |                   |
 miniwind | orders_id_seq        | sequence |                     |                   |
 miniwind | orders_status        | table    |                     |                   |
 miniwind | products             | table    | admin=arwdDxt/admin+|                   |
          |                      |          | sabadell=r/admin    |                   |
 miniwind | products_id_seq      | sequence |                     |                   |
 miniwind | suppliers            | table    |                     |                   |
 miniwind | suppliers_id_seq     | sequence |                     |                   |
 (16 rows)
----
+
[source,sql]
----
miniwind=> select product_name from products limit 1;
      product_name
------------------------
 Northwind Traders Chai
(1 row)
----

15. Esborra tots els usuaris creats.
+
[source,sql]
----
postgres=# drop role sabadell, terrassa;
ERROR:  role "sabadell" cannot be dropped because some objects depend on it
DETAIL:  2 objects in database miniwind
14 objects in database hotel
postgres=# \c miniwind
You are now connected to database "miniwind" as user "postgres".
miniwind=# revoke all on schema miniwind from sabadell;
REVOKE
miniwind=# revoke all on all tables in schema miniwind from sabadell;
REVOKE
miniwind=# \c hotel
You are now connected to database "hotel" as user "postgres".
hotel=# revoke all on schema hotel from sabadell;
REVOKE
hotel=# revoke all on all tables in schema hotel from sabadell;
REVOKE
hotel=# drop role sabadell, terrassa;
ERROR:  role "sabadell" cannot be dropped because some objects depend on it
DETAIL:  privileges for sequence bookings_id_seq
hotel=# revoke all on sequence bookings_id_seq from sabadell;
REVOKE
hotel=# drop role sabadell, terrassa;
DROP ROLE
----
