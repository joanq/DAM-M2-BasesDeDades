= Eleccions
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Model relacional

*Voters*([underline]#DNI#, FirstName, LastNames, BirthDate, Code, Position, HasVoted, AddressId) +
{nbsp}{nbsp}on _Code_ referencia _PollingBooths_ i +
{nbsp}{nbsp}on _AddressId_ referencia _Addresses_.

*Addresses*([underline]#AddressId#, AddressLine1, AddressLine2, ZipCode, City, Country, Province) +
{nbsp}{nbsp}on _Province_ referencia _Provinces_.

*Provinces*([underline]#Name#)

*PollingBooths*([underline]#Code#, AddressId) +
{nbsp}{nbsp}on _AddressId_ referencia _Addresses_.

*Parties*([underline]#Name#, NumMembers, Budget, AddressId, Candidacy) +
{nbsp}{nbsp}on _AddressId_ referencia _Addresses_ i +
{nbsp}{nbsp}on _Candidacy_ referencia _Voters_.

*Positions*([underline]#Name#)

*RelatedParties*([underline]#Party1, Party2#) +
{nbsp}{nbsp}on _Party1_ referencia _Parties_ i +
{nbsp}{nbsp}on _Party2_ referencia _Parties_.

*Jobs*([underline]#DNI, Position#, Party, Salary) +
{nbsp}{nbsp}on _DNI_ referencia _Voters_, +
{nbsp}{nbsp}on _Position_ referencia _Positions_, i +
{nbsp}{nbsp}on _Party_ referencia _Parties_.
