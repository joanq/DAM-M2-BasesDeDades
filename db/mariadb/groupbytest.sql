drop database if exists groupbytest;
create database groupbytest;
use groupbytest;
create table Employees (
	Id INT UNSIGNED PRIMARY KEY,
	FirstName VARCHAR(100) NOT NULL,
	LastName VARCHAR(100) NOT NULL,
	Salary DECIMAL(6,2) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4;
insert into Employees values (1, 'Maria', 'Martínez', 1700);
insert into Employees values (2, 'Pere', 'Prat', 1100);
insert into Employees values (3, 'Pere', 'Garcia', 1500);
insert into Employees values (4, 'Marta', 'Muñoz', 1100);

create table Employees2 (
	Id INT UNSIGNED PRIMARY KEY,
	FirstName VARCHAR(100) NOT NULL,
	LastName VARCHAR(100) NOT NULL,
	Salary DECIMAL(6,2) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4;
INSERT INTO Employees2 VALUES
(1, 'Maria', 'Martínez', 1700.00),
(2, 'Pere', 'Prat', 1100.00),
(3, 'Pere', 'Garcia', 1500.00),
(4, 'Marta', 'Muñoz', 1100.00),
(5, 'Maria', 'Herrera', 1700.00),
(6, 'Pere', 'Rovira', 1500.00),
(7, 'Miquel', 'Armenter', 1700.00),
(8, 'Sílvia', 'Barba', 1500.00),
(9, 'Pere', 'Serrano', 1100.00),
(10, 'Marta', 'Perich', 1400.00),
(11, 'Marta', 'Nogué', 1100.00);

create table Employees3 (
	Id INT UNSIGNED PRIMARY KEY,
	FirstName VARCHAR(100) NOT NULL,
	LastName VARCHAR(100) NOT NULL,
	Salary DECIMAL(6,2) UNSIGNED
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4;
INSERT INTO Employees3 VALUES
(1, 'Maria', 'Martínez', 1700.00),
(2, 'Pere', 'Prat', 1100.00),
(3, 'Pere', 'Garcia', 1500.00),
(4, 'Marta', 'Muñoz', 1100.00),
(5, 'Maria', 'Herrera', 1700.00),
(6, 'Pere', 'Rovira', 1500.00),
(7, 'Miquel', 'Armenter', 1700.00),
(8, 'Sílvia', 'Barba', 1500.00),
(9, 'Pere', 'Serrano', NULL),
(10, 'Marta', 'Perich', NULL),
(11, 'Marta', 'Nogué', 1100.00);
