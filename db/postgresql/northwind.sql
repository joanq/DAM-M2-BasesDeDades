--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE northwind;
--
-- Name: northwind; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE northwind WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


\connect northwind

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: northwind; Type: DATABASE PROPERTIES; Schema: -; Owner: -
--

ALTER DATABASE northwind SET search_path TO 'public', 'northwind';


\connect northwind

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: northwind; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA northwind;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customers; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.customers (
    id bigint NOT NULL,
    company character varying(50) DEFAULT NULL::character varying,
    last_name character varying(50) DEFAULT NULL::character varying,
    first_name character varying(50) DEFAULT NULL::character varying,
    email_address character varying(50) DEFAULT NULL::character varying,
    job_title character varying(50) DEFAULT NULL::character varying,
    business_phone character varying(25) DEFAULT NULL::character varying,
    home_phone character varying(25) DEFAULT NULL::character varying,
    mobile_phone character varying(25) DEFAULT NULL::character varying,
    fax_number character varying(25) DEFAULT NULL::character varying,
    address text,
    city character varying(50) DEFAULT NULL::character varying,
    state_province character varying(50) DEFAULT NULL::character varying,
    zip_postal_code character varying(15) DEFAULT NULL::character varying,
    country_region character varying(50) DEFAULT NULL::character varying,
    web_page text,
    notes text
);


--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.customers_id_seq OWNED BY northwind.customers.id;


--
-- Name: employee_privileges; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.employee_privileges (
    employee_id bigint NOT NULL,
    privilege_id bigint NOT NULL
);


--
-- Name: employees; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.employees (
    id bigint NOT NULL,
    company character varying(50) DEFAULT NULL::character varying,
    last_name character varying(50) DEFAULT NULL::character varying,
    first_name character varying(50) DEFAULT NULL::character varying,
    email_address character varying(50) DEFAULT NULL::character varying,
    job_title character varying(50) DEFAULT NULL::character varying,
    business_phone character varying(25) DEFAULT NULL::character varying,
    home_phone character varying(25) DEFAULT NULL::character varying,
    mobile_phone character varying(25) DEFAULT NULL::character varying,
    fax_number character varying(25) DEFAULT NULL::character varying,
    address text,
    city character varying(50) DEFAULT NULL::character varying,
    state_province character varying(50) DEFAULT NULL::character varying,
    zip_postal_code character varying(15) DEFAULT NULL::character varying,
    country_region character varying(50) DEFAULT NULL::character varying,
    web_page text,
    notes text
);


--
-- Name: employees_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.employees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employees_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.employees_id_seq OWNED BY northwind.employees.id;


--
-- Name: inventory_transaction_types; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.inventory_transaction_types (
    id smallint NOT NULL,
    type_name character varying(50) NOT NULL
);


--
-- Name: inventory_transactions; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.inventory_transactions (
    id bigint NOT NULL,
    transaction_type smallint NOT NULL,
    transaction_created_date timestamp with time zone,
    transaction_modified_date timestamp with time zone,
    product_id bigint NOT NULL,
    quantity bigint NOT NULL,
    purchase_order_id bigint,
    customer_order_id bigint,
    comments character varying(255) DEFAULT NULL::character varying
);


--
-- Name: inventory_transactions_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.inventory_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inventory_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.inventory_transactions_id_seq OWNED BY northwind.inventory_transactions.id;


--
-- Name: invoices; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.invoices (
    id bigint NOT NULL,
    order_id bigint,
    invoice_date timestamp with time zone,
    due_date timestamp with time zone,
    tax numeric(19,4) DEFAULT 0.0000,
    shipping numeric(19,4) DEFAULT 0.0000,
    amount_due numeric(19,4) DEFAULT 0.0000
);


--
-- Name: invoices_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.invoices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.invoices_id_seq OWNED BY northwind.invoices.id;


--
-- Name: order_details; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.order_details (
    id bigint NOT NULL,
    order_id bigint NOT NULL,
    product_id bigint,
    quantity numeric(18,4) DEFAULT 0.0000 NOT NULL,
    unit_price numeric(19,4) DEFAULT 0.0000,
    discount double precision DEFAULT '0'::double precision NOT NULL,
    status_id bigint,
    date_allocated timestamp with time zone,
    purchase_order_id bigint,
    inventory_id bigint
);


--
-- Name: order_details_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_details_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.order_details_id_seq OWNED BY northwind.order_details.id;


--
-- Name: order_details_status; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.order_details_status (
    id bigint NOT NULL,
    status_name character varying(50) NOT NULL
);


--
-- Name: orders; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.orders (
    id bigint NOT NULL,
    employee_id bigint,
    customer_id bigint,
    order_date timestamp with time zone,
    shipped_date timestamp with time zone,
    shipper_id bigint,
    ship_name character varying(50) DEFAULT NULL::character varying,
    ship_address text,
    ship_city character varying(50) DEFAULT NULL::character varying,
    ship_state_province character varying(50) DEFAULT NULL::character varying,
    ship_zip_postal_code character varying(50) DEFAULT NULL::character varying,
    ship_country_region character varying(50) DEFAULT NULL::character varying,
    shipping_fee numeric(19,4) DEFAULT 0.0000,
    taxes numeric(19,4) DEFAULT 0.0000,
    payment_type character varying(50) DEFAULT NULL::character varying,
    paid_date timestamp with time zone,
    notes text,
    tax_rate double precision DEFAULT '0'::double precision,
    tax_status_id smallint,
    status_id smallint DEFAULT '0'::smallint
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.orders_id_seq OWNED BY northwind.orders.id;


--
-- Name: orders_status; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.orders_status (
    id smallint NOT NULL,
    status_name character varying(50) NOT NULL
);


--
-- Name: orders_tax_status; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.orders_tax_status (
    id smallint NOT NULL,
    tax_status_name character varying(50) NOT NULL
);


--
-- Name: privileges; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.privileges (
    id bigint NOT NULL,
    privilege_name character varying(50) DEFAULT NULL::character varying
);


--
-- Name: privileges_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.privileges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privileges_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.privileges_id_seq OWNED BY northwind.privileges.id;


--
-- Name: products; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.products (
    supplier_ids text,
    id bigint NOT NULL,
    product_code character varying(25) DEFAULT NULL::character varying,
    product_name character varying(50) DEFAULT NULL::character varying,
    description text,
    standard_cost numeric(19,4) DEFAULT 0.0000,
    list_price numeric(19,4) DEFAULT 0.0000 NOT NULL,
    reorder_level bigint,
    target_level bigint,
    quantity_per_unit character varying(50) DEFAULT NULL::character varying,
    discontinued boolean DEFAULT false NOT NULL,
    minimum_reorder_quantity bigint,
    category character varying(50) DEFAULT NULL::character varying,
    attachments bytea
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.products_id_seq OWNED BY northwind.products.id;


--
-- Name: purchase_order_details; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.purchase_order_details (
    id bigint NOT NULL,
    purchase_order_id bigint NOT NULL,
    product_id bigint,
    quantity numeric(18,4) NOT NULL,
    unit_cost numeric(19,4) NOT NULL,
    date_received timestamp with time zone,
    posted_to_inventory boolean DEFAULT false NOT NULL,
    inventory_id bigint
);


--
-- Name: purchase_order_details_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.purchase_order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: purchase_order_details_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.purchase_order_details_id_seq OWNED BY northwind.purchase_order_details.id;


--
-- Name: purchase_order_status; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.purchase_order_status (
    id bigint NOT NULL,
    status character varying(50) DEFAULT NULL::character varying
);


--
-- Name: purchase_orders; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.purchase_orders (
    id bigint NOT NULL,
    supplier_id bigint,
    created_by bigint,
    submitted_date timestamp with time zone,
    creation_date timestamp with time zone,
    status_id bigint DEFAULT '0'::bigint,
    expected_date timestamp with time zone,
    shipping_fee numeric(19,4) DEFAULT 0.0000 NOT NULL,
    taxes numeric(19,4) DEFAULT 0.0000 NOT NULL,
    payment_date timestamp with time zone,
    payment_amount numeric(19,4) DEFAULT 0.0000,
    payment_method character varying(50) DEFAULT NULL::character varying,
    notes text,
    approved_by bigint,
    approved_date timestamp with time zone,
    submitted_by bigint
);


--
-- Name: purchase_orders_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.purchase_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: purchase_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.purchase_orders_id_seq OWNED BY northwind.purchase_orders.id;


--
-- Name: sales_reports; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.sales_reports (
    group_by character varying(50) NOT NULL,
    display character varying(50) DEFAULT NULL::character varying,
    title character varying(50) DEFAULT NULL::character varying,
    filter_row_source text,
    "default" boolean DEFAULT false NOT NULL
);


--
-- Name: shippers; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.shippers (
    id bigint NOT NULL,
    company character varying(50) DEFAULT NULL::character varying,
    last_name character varying(50) DEFAULT NULL::character varying,
    first_name character varying(50) DEFAULT NULL::character varying,
    email_address character varying(50) DEFAULT NULL::character varying,
    job_title character varying(50) DEFAULT NULL::character varying,
    business_phone character varying(25) DEFAULT NULL::character varying,
    home_phone character varying(25) DEFAULT NULL::character varying,
    mobile_phone character varying(25) DEFAULT NULL::character varying,
    fax_number character varying(25) DEFAULT NULL::character varying,
    address text,
    city character varying(50) DEFAULT NULL::character varying,
    state_province character varying(50) DEFAULT NULL::character varying,
    zip_postal_code character varying(15) DEFAULT NULL::character varying,
    country_region character varying(50) DEFAULT NULL::character varying,
    web_page text,
    notes text,
    attachments bytea
);


--
-- Name: shippers_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.shippers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shippers_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.shippers_id_seq OWNED BY northwind.shippers.id;


--
-- Name: strings; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.strings (
    string_id bigint NOT NULL,
    string_data character varying(255) DEFAULT NULL::character varying
);


--
-- Name: strings_string_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.strings_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: strings_string_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.strings_string_id_seq OWNED BY northwind.strings.string_id;


--
-- Name: suppliers; Type: TABLE; Schema: northwind; Owner: -
--

CREATE TABLE northwind.suppliers (
    id bigint NOT NULL,
    company character varying(50) DEFAULT NULL::character varying,
    last_name character varying(50) DEFAULT NULL::character varying,
    first_name character varying(50) DEFAULT NULL::character varying,
    email_address character varying(50) DEFAULT NULL::character varying,
    job_title character varying(50) DEFAULT NULL::character varying,
    business_phone character varying(25) DEFAULT NULL::character varying,
    home_phone character varying(25) DEFAULT NULL::character varying,
    mobile_phone character varying(25) DEFAULT NULL::character varying,
    fax_number character varying(25) DEFAULT NULL::character varying,
    address text,
    city character varying(50) DEFAULT NULL::character varying,
    state_province character varying(50) DEFAULT NULL::character varying,
    zip_postal_code character varying(15) DEFAULT NULL::character varying,
    country_region character varying(50) DEFAULT NULL::character varying,
    web_page text,
    notes text,
    attachments bytea
);


--
-- Name: suppliers_id_seq; Type: SEQUENCE; Schema: northwind; Owner: -
--

CREATE SEQUENCE northwind.suppliers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: suppliers_id_seq; Type: SEQUENCE OWNED BY; Schema: northwind; Owner: -
--

ALTER SEQUENCE northwind.suppliers_id_seq OWNED BY northwind.suppliers.id;


--
-- Name: customers id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.customers ALTER COLUMN id SET DEFAULT nextval('northwind.customers_id_seq'::regclass);


--
-- Name: employees id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.employees ALTER COLUMN id SET DEFAULT nextval('northwind.employees_id_seq'::regclass);


--
-- Name: inventory_transactions id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions ALTER COLUMN id SET DEFAULT nextval('northwind.inventory_transactions_id_seq'::regclass);


--
-- Name: invoices id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.invoices ALTER COLUMN id SET DEFAULT nextval('northwind.invoices_id_seq'::regclass);


--
-- Name: order_details id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details ALTER COLUMN id SET DEFAULT nextval('northwind.order_details_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders ALTER COLUMN id SET DEFAULT nextval('northwind.orders_id_seq'::regclass);


--
-- Name: privileges id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.privileges ALTER COLUMN id SET DEFAULT nextval('northwind.privileges_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.products ALTER COLUMN id SET DEFAULT nextval('northwind.products_id_seq'::regclass);


--
-- Name: purchase_order_details id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_details ALTER COLUMN id SET DEFAULT nextval('northwind.purchase_order_details_id_seq'::regclass);


--
-- Name: purchase_orders id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_orders ALTER COLUMN id SET DEFAULT nextval('northwind.purchase_orders_id_seq'::regclass);


--
-- Name: shippers id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.shippers ALTER COLUMN id SET DEFAULT nextval('northwind.shippers_id_seq'::regclass);


--
-- Name: strings string_id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.strings ALTER COLUMN string_id SET DEFAULT nextval('northwind.strings_string_id_seq'::regclass);


--
-- Name: suppliers id; Type: DEFAULT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.suppliers ALTER COLUMN id SET DEFAULT nextval('northwind.suppliers_id_seq'::regclass);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.customers (id, company, last_name, first_name, email_address, job_title, business_phone, home_phone, mobile_phone, fax_number, address, city, state_province, zip_postal_code, country_region, web_page, notes) FROM stdin;
1	Lueilwitz, Homenick and Quigley	Gladhill	Marga	mgladhill0@constantcontact.com	Purchasing Representative	(218) 7543592	(502) 1969465	(785) 5471951	(608) 2548372	63202 Aberg Hill	Duluth	MN	55811	USA	http://nba.com	Ergonomic coherent architecture
2	Toy-Rutherford	Brett	Marco	mbrett1@unicef.org	Purchasing Representative	(714) 1838692	(408) 7268462	(817) 7965289	\N	7 Homewood Road	Santa Ana	CA	92705	USA	https://ebay.co.uk	\N
3	Lueilwitz-Zboncak	Bellelli	Trixie	tbellelli2@biblegateway.com	Accounting Assistant	(504) 6117320	(954) 5514448	(571) 4007887	\N	17 Quincy Plaza	New Orleans	LA	70183	USA	https://drupal.org	\N
4	Renner, Beier and Denesik	Teece	Marnie	mteece3@studiopress.com	Purchasing Representative	(417) 4647412	\N	(786) 2516698	\N	9152 Lake View Trail	Springfield	MO	65805	USA	https://etsy.com	\N
5	Ryan, Farrell and Tromp	Sellers	Dredi	dsellers4@blogger.com	Purchasing Representative	(757) 7809889	(213) 4037331	(559) 4379480	(412) 9312354	2024 Pawling Circle	Virginia Beach	VA	23471	USA	https://noaa.gov	Phased solution-oriented interface
6	Ullrich, Schoen and Ankunding	Squirrel	Emma	esquirrel5@youtube.com	Purchasing Representative	(727) 6494680	(941) 3409354	(410) 3701720	(202) 9871884	86 Ridgeway Crossing	Largo	FL	34643	USA	http://huffingtonpost.com	Proactive non-volatile groupware
7	Hudson-Lesch	Dyott	Joni	jdyott6@foxnews.com	Owner	(317) 2109659	(775) 2741877	(509) 3210210	\N	7258 Heffernan Parkway	Indianapolis	IN	46247	USA	https://gravatar.com	\N
8	Emmerich, Mueller and Volkman	O'Lynn	Lorianne	lolynn7@example.com	Purchasing Representative	(405) 9147172	(202) 1229655	(585) 8222889	\N	2 Commercial Road	Oklahoma City	OK	73152	USA	http://gizmodo.com	\N
9	Zemlak-Muller	Trew	Mendel	mtrew8@mozilla.com	Owner	(937) 4626023	\N	(336) 4148918	(770) 1917693	442 Lukken Circle	Dayton	OH	45403	USA	https://tuttocitta.it	Extended 24 hour info-mediaries
10	Gerhold-Carter	Talks	Tessie	ttalks9@cdc.gov	Purchasing Representative	(904) 3145350	(559) 8789016	(818) 8653483	(518) 6701602	29 Texas Road	Jacksonville	FL	32230	USA	https://princeton.edu	Virtual responsive local area network
11	Towne, Mante and Kuphal	Fuzzens	Rhody	rfuzzensa@istockphoto.com	Purchasing Representative	(843) 6352893	(563) 7133392	(941) 2995824	(918) 4201953	7 Steensland Street	Myrtle Beach	SC	29579	USA	https://uiuc.edu	Advanced cohesive migration
12	Larson-Weber	Clohisey	Concordia	cclohiseyb@disqus.com	Purchasing Manager	(559) 4022961	(229) 2856456	(937) 8628987	\N	4686 Harbort Court	Fresno	CA	93740	USA	http://geocities.com	\N
13	Wiza-Wisozk	Bonnaire	Orelia	obonnairec@go.com	Purchasing Manager	(713) 1326126	(215) 9162910	(972) 5298537	\N	99589 Lerdahl Trail	Houston	TX	77045	USA	http://mapquest.com	\N
14	Mante-Wintheiser	Malley	Pieter	pmalleyd@fda.gov	Purchasing Representative	(412) 7635338	\N	(915) 5775902	(901) 1403016	025 Arapahoe Court	Pittsburgh	PA	15279	USA	https://youtu.be	Inverse stable focus group
15	McDermott-Spinka	Streat	Ammamaria	astreate@seattletimes.com	Accounting Assistant	(915) 7070771	(520) 5440930	(314) 2160121	\N	12 Scott Parkway	El Paso	TX	79955	USA	http://qq.com	\N
16	McLaughlin, Weber and Green	Ashton	Thorndike	tashtonf@rambler.ru	Owner	(646) 6973210	(330) 1750629	(513) 8346641	\N	1 Magdeline Lane	New York City	NY	10155	USA	http://amazon.de	\N
17	Welch Group	Ludlem	Mace	mludlemg@cloudflare.com	Accounting Assistant	(251) 8035067	\N	(801) 3456443	(240) 6516105	979 Pennsylvania Hill	Mobile	AL	36628	USA	http://ihg.com	Inverse static array
18	Treutel LLC	Fugere	Kasey	kfugereh@tinypic.com	Purchasing Manager	(361) 8034572	\N	(352) 6247543	(517) 7469447	5 Melvin Way	Corpus Christi	TX	78410	USA	http://apple.com	Decentralized hybrid instruction set
19	Hoeger-Ernser	Kindle	Shandeigh	skindlei@theatlantic.com	Purchasing Representative	(619) 7423144	\N	(305) 9024633	\N	5668 Anhalt Plaza	San Diego	CA	92121	USA	https://nature.com	\N
20	Buckridge LLC	Willis	Gipsy	gwillisj@symantec.com	Purchasing Manager	(859) 6736790	(850) 3020223	(419) 7453729	\N	53 Gulseth Street	Lexington	KY	40591	USA	http://pinterest.com	\N
21	Mitchell, Brakus and Bradtke	Gonnel	Filberto	fgonnelk@pinterest.com	Owner	(704) 3442340	(303) 8887638	(419) 9832396	(775) 4153950	9 Hoard Junction	Charlotte	NC	28284	USA	http://ibm.com	Multi-channelled impactful installation
22	Lockman, Pagac and Doyle	Messham	Dolley	dmesshaml@furl.net	Purchasing Manager	(913) 5354323	\N	(770) 1848252	\N	59 Cardinal Court	Shawnee Mission	KS	66286	USA	https://wunderground.com	\N
23	Franecki and Sons	Sabin	Jude	jsabinm@live.com	Purchasing Manager	(559) 7298955	(612) 6154057	(860) 5354017	\N	71 Mayer Place	Fresno	CA	93704	USA	https://vimeo.com	\N
24	Kohler and Sons	Barron	Hannie	hbarronn@clickbank.net	Purchasing Manager	(718) 5577645	(913) 8621064	(706) 4281660	\N	07 Hermina Point	Brooklyn	NY	11225	USA	https://patch.com	\N
25	Mayert Inc	Murden	Alford	amurdeno@sina.com.cn	Purchasing Representative	(605) 8389189	(701) 1682311	(813) 9777076	(910) 1729114	59546 Wayridge Street	Sioux Falls	SD	57105	USA	https://xrea.com	Triple-buffered demand-driven moratorium
26	Mertz, Weimann and Durgan	Basnett	Agata	abasnettp@weather.com	Accounting Assistant	(502) 6251630	\N	(502) 8439841	\N	1842 Mesta Lane	Louisville	KY	40210	USA	https://reuters.com	\N
27	Sporer LLC	Balkwill	Judye	jbalkwillq@facebook.com	Purchasing Representative	(260) 1781142	\N	(918) 6821657	(315) 6699250	0 Stephen Crossing	Fort Wayne	IN	46852	USA	http://kickstarter.com	Proactive context-sensitive hierarchy
28	Feil, Mills and Koss	Mussington	Sumner	smussingtonr@umn.edu	Owner	(916) 5926437	(304) 6862625	(404) 2902575	(904) 9977319	336 Arapahoe Way	Sacramento	CA	94250	USA	https://toplist.cz	Team-oriented fresh-thinking functionalities
29	Cruickshank Inc	Coulthard	Belita	bcoulthards@wsj.com	Accounting Assistant	(317) 9102437	\N	(408) 9337973	(602) 6914579	49 Mayfield Pass	Indianapolis	IN	46295	USA	http://usgs.gov	Self-enabling exuding software
\.


--
-- Data for Name: employee_privileges; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.employee_privileges (employee_id, privilege_id) FROM stdin;
2	2
\.


--
-- Data for Name: employees; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.employees (id, company, last_name, first_name, email_address, job_title, business_phone, home_phone, mobile_phone, fax_number, address, city, state_province, zip_postal_code, country_region, web_page, notes) FROM stdin;
1	Northwind Traders	Jurek	Maynord	maynord@northwindtraders.com	Sales Representative	(503) 3056466	(910) 2453215	(901) 3049980	\N	026 Ohio Alley	Portland	OR	97296	USA	http://northwindtraders.com	\N
2	Northwind Traders	Guyton	Jabez	jabez@northwindtraders.com	Vice President, Sales	(559) 8341361	\N	(608) 4693824	\N	96050 Helena Terrace	Fresno	CA	93740	USA	http://northwindtraders.com	Joined the company as a sales representative, was promoted to sales manager and was then named vice president of sales.
3	Northwind Traders	Pointing	Hall	hall@northwindtraders.com	Sales Representative	(915) 6692616	\N	(614) 7124463	\N	0651 Melvin Way	El Paso	TX	79911	USA	http://northwindtraders.com	Was hired as a sales associate and was promoted to sales representative.
4	Northwind Traders	O Sullivan	Delaney	delaney@northwindtraders.com	Sales Representative	(856) 6962615	(478) 9288298	(330) 5001704	(717) 5282168	5787 Schlimgen Circle	Camden	NJ	08104	USA	http://northwindtraders.com	\N
5	Northwind Traders	Coppin	Grover	grover@northwindtraders.com	Sales Manager	(770) 3736397	\N	(904) 2943765	\N	83 Mariners Cove Way	Atlanta	GA	30301	USA	http://northwindtraders.com	Joined the company as a sales representative and was promoted to sales manager.  Fluent in French.
6	Northwind Traders	Seczyk	Cesya	cesya@northwindtraders.com	Sales Representative	(405) 5659782	\N	(253) 8468633	\N	77 Dakota Plaza	Oklahoma City	OK	73142	USA	http://northwindtraders.com	Fluent in Japanese and can read and write French, Portuguese, and Spanish.
7	Northwind Traders	Redit	Titus	titus@northwindtraders.com	Sales Representative	(202) 8665604	(786) 5124323	(734) 4105611	\N	94704 Oak Valley Circle	Washington	DC	20425	USA	http://northwindtraders.com	\N
8	Northwind Traders	Jirusek	Thelma	thelma@northwindtraders.com	Sales Coordinator	(806) 7944076	(989) 7157273	(607) 7982119	(601) 4964138	1761 Farwell Circle	Lubbock	TX	79491	USA	http://northwindtraders.com	Reads and writes French.
9	Northwind Traders	Polet	Selia	selia@northwindtraders.com	Sales Representative	(202) 9632160	(304) 1191047	(202) 7115116	\N	0241 Sutteridge Crossing	Washington	DC	20022	USA	http://northwindtraders.com	Fluent in French and German.
\.


--
-- Data for Name: inventory_transaction_types; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.inventory_transaction_types (id, type_name) FROM stdin;
1	Purchased
2	Sold
3	On Hold
4	Waste
\.


--
-- Data for Name: inventory_transactions; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.inventory_transactions (id, transaction_type, transaction_created_date, transaction_modified_date, product_id, quantity, purchase_order_id, customer_order_id, comments) FROM stdin;
35	1	2006-03-22 16:02:28+00	2006-03-22 16:02:28+00	80	75	\N	\N	\N
36	1	2006-03-22 16:02:48+00	2006-03-22 16:02:48+00	72	40	\N	\N	\N
37	1	2006-03-22 16:03:04+00	2006-03-22 16:03:04+00	52	100	\N	\N	\N
38	1	2006-03-22 16:03:09+00	2006-03-22 16:03:09+00	56	120	\N	\N	\N
39	1	2006-03-22 16:03:14+00	2006-03-22 16:03:14+00	57	80	\N	\N	\N
40	1	2006-03-22 16:03:40+00	2006-03-22 16:03:40+00	6	100	\N	\N	\N
41	1	2006-03-22 16:03:47+00	2006-03-22 16:03:47+00	7	40	\N	\N	\N
42	1	2006-03-22 16:03:54+00	2006-03-22 16:03:54+00	8	40	\N	\N	\N
43	1	2006-03-22 16:04:02+00	2006-03-22 16:04:02+00	14	40	\N	\N	\N
44	1	2006-03-22 16:04:07+00	2006-03-22 16:04:07+00	17	40	\N	\N	\N
45	1	2006-03-22 16:04:12+00	2006-03-22 16:04:12+00	19	20	\N	\N	\N
46	1	2006-03-22 16:04:17+00	2006-03-22 16:04:17+00	20	40	\N	\N	\N
47	1	2006-03-22 16:04:20+00	2006-03-22 16:04:20+00	21	20	\N	\N	\N
48	1	2006-03-22 16:04:24+00	2006-03-22 16:04:24+00	40	120	\N	\N	\N
49	1	2006-03-22 16:04:28+00	2006-03-22 16:04:28+00	41	40	\N	\N	\N
50	1	2006-03-22 16:04:31+00	2006-03-22 16:04:31+00	48	100	\N	\N	\N
51	1	2006-03-22 16:04:38+00	2006-03-22 16:04:38+00	51	40	\N	\N	\N
52	1	2006-03-22 16:04:41+00	2006-03-22 16:04:41+00	74	20	\N	\N	\N
53	1	2006-03-22 16:04:45+00	2006-03-22 16:04:45+00	77	60	\N	\N	\N
54	1	2006-03-22 16:05:07+00	2006-03-22 16:05:07+00	3	100	\N	\N	\N
55	1	2006-03-22 16:05:11+00	2006-03-22 16:05:11+00	4	40	\N	\N	\N
56	1	2006-03-22 16:05:14+00	2006-03-22 16:05:14+00	5	40	\N	\N	\N
57	1	2006-03-22 16:05:26+00	2006-03-22 16:05:26+00	65	40	\N	\N	\N
58	1	2006-03-22 16:05:32+00	2006-03-22 16:05:32+00	66	80	\N	\N	\N
59	1	2006-03-22 16:05:47+00	2006-03-22 16:05:47+00	1	40	\N	\N	\N
60	1	2006-03-22 16:05:51+00	2006-03-22 16:05:51+00	34	60	\N	\N	\N
61	1	2006-03-22 16:06:00+00	2006-03-22 16:06:00+00	43	100	\N	\N	\N
62	1	2006-03-22 16:06:03+00	2006-03-22 16:06:03+00	81	125	\N	\N	\N
63	2	2006-03-22 16:07:56+00	2006-03-24 11:03:00+00	80	30	\N	\N	\N
64	2	2006-03-22 16:08:19+00	2006-03-22 16:08:59+00	7	10	\N	\N	\N
65	2	2006-03-22 16:08:29+00	2006-03-22 16:08:59+00	51	10	\N	\N	\N
66	2	2006-03-22 16:08:37+00	2006-03-22 16:08:59+00	80	10	\N	\N	\N
67	2	2006-03-22 16:09:46+00	2006-03-22 16:10:27+00	1	15	\N	\N	\N
68	2	2006-03-22 16:10:06+00	2006-03-22 16:10:27+00	43	20	\N	\N	\N
69	2	2006-03-22 16:11:39+00	2006-03-24 11:00:55+00	19	20	\N	\N	\N
70	2	2006-03-22 16:11:56+00	2006-03-24 10:59:41+00	48	10	\N	\N	\N
71	2	2006-03-22 16:12:29+00	2006-03-24 10:57:38+00	8	17	\N	\N	\N
72	1	2006-03-24 10:41:30+00	2006-03-24 10:41:30+00	81	200	\N	\N	\N
73	2	2006-03-24 10:41:33+00	2006-03-24 10:41:42+00	81	200	\N	\N	Fill Back Ordered product, Order #40
74	1	2006-03-24 10:53:13+00	2006-03-24 10:53:13+00	48	100	\N	\N	\N
75	2	2006-03-24 10:53:16+00	2006-03-24 10:55:46+00	48	100	\N	\N	Fill Back Ordered product, Order #39
76	1	2006-03-24 10:53:36+00	2006-03-24 10:53:36+00	43	300	\N	\N	\N
77	2	2006-03-24 10:53:39+00	2006-03-24 10:56:57+00	43	300	\N	\N	Fill Back Ordered product, Order #38
78	1	2006-03-24 10:54:04+00	2006-03-24 10:54:04+00	41	200	\N	\N	\N
79	2	2006-03-24 10:54:07+00	2006-03-24 10:58:40+00	41	200	\N	\N	Fill Back Ordered product, Order #36
80	1	2006-03-24 10:54:33+00	2006-03-24 10:54:33+00	19	30	\N	\N	\N
81	2	2006-03-24 10:54:35+00	2006-03-24 11:02:02+00	19	30	\N	\N	Fill Back Ordered product, Order #33
82	1	2006-03-24 10:54:58+00	2006-03-24 10:54:58+00	34	100	\N	\N	\N
83	2	2006-03-24 10:55:02+00	2006-03-24 11:03:00+00	34	100	\N	\N	Fill Back Ordered product, Order #30
84	2	2006-03-24 14:48:15+00	2006-04-04 11:41:14+00	6	10	\N	\N	\N
85	2	2006-03-24 14:48:23+00	2006-04-04 11:41:14+00	4	10	\N	\N	\N
86	3	2006-03-24 14:49:16+00	2006-03-24 14:49:16+00	80	20	\N	\N	\N
87	3	2006-03-24 14:49:20+00	2006-03-24 14:49:20+00	81	50	\N	\N	\N
88	3	2006-03-24 14:50:09+00	2006-03-24 14:50:09+00	1	25	\N	\N	\N
89	3	2006-03-24 14:50:14+00	2006-03-24 14:50:14+00	43	25	\N	\N	\N
90	3	2006-03-24 14:50:18+00	2006-03-24 14:50:18+00	81	25	\N	\N	\N
91	2	2006-03-24 14:51:03+00	2006-04-04 11:09:24+00	40	50	\N	\N	\N
92	2	2006-03-24 14:55:03+00	2006-04-04 11:06:56+00	21	20	\N	\N	\N
93	2	2006-03-24 14:55:39+00	2006-04-04 11:06:13+00	5	25	\N	\N	\N
94	2	2006-03-24 14:55:52+00	2006-04-04 11:06:13+00	41	30	\N	\N	\N
95	2	2006-03-24 14:56:09+00	2006-04-04 11:06:13+00	40	30	\N	\N	\N
96	3	2006-03-30 16:46:34+00	2006-03-30 16:46:34+00	34	12	\N	\N	\N
97	3	2006-03-30 17:23:27+00	2006-03-30 17:23:27+00	34	10	\N	\N	\N
98	3	2006-03-30 17:24:33+00	2006-03-30 17:24:33+00	34	1	\N	\N	\N
99	2	2006-04-03 13:50:08+00	2006-04-03 13:50:15+00	48	10	\N	\N	\N
100	1	2006-04-04 11:00:54+00	2006-04-04 11:00:54+00	57	100	\N	\N	\N
101	2	2006-04-04 11:00:56+00	2006-04-04 11:08:49+00	57	100	\N	\N	Fill Back Ordered product, Order #46
102	1	2006-04-04 11:01:14+00	2006-04-04 11:01:14+00	34	50	\N	\N	\N
103	1	2006-04-04 11:01:35+00	2006-04-04 11:01:35+00	43	250	\N	\N	\N
104	3	2006-04-04 11:01:37+00	2006-04-04 11:01:37+00	43	300	\N	\N	Fill Back Ordered product, Order #41
105	1	2006-04-04 11:01:55+00	2006-04-04 11:01:55+00	8	25	\N	\N	\N
106	2	2006-04-04 11:01:58+00	2006-04-04 11:07:37+00	8	25	\N	\N	Fill Back Ordered product, Order #48
107	1	2006-04-04 11:02:17+00	2006-04-04 11:02:17+00	34	300	\N	\N	\N
108	2	2006-04-04 11:02:19+00	2006-04-04 11:08:14+00	34	300	\N	\N	Fill Back Ordered product, Order #47
109	1	2006-04-04 11:02:37+00	2006-04-04 11:02:37+00	19	25	\N	\N	\N
110	2	2006-04-04 11:02:39+00	2006-04-04 11:41:14+00	19	10	\N	\N	Fill Back Ordered product, Order #42
111	1	2006-04-04 11:02:56+00	2006-04-04 11:02:56+00	19	10	\N	\N	\N
112	2	2006-04-04 11:02:58+00	2006-04-04 11:07:37+00	19	25	\N	\N	Fill Back Ordered product, Order #48
113	1	2006-04-04 11:03:12+00	2006-04-04 11:03:12+00	72	50	\N	\N	\N
114	2	2006-04-04 11:03:14+00	2006-04-04 11:08:49+00	72	50	\N	\N	Fill Back Ordered product, Order #46
115	1	2006-04-04 11:03:38+00	2006-04-04 11:03:38+00	41	50	\N	\N	\N
116	2	2006-04-04 11:03:39+00	2006-04-04 11:09:24+00	41	50	\N	\N	Fill Back Ordered product, Order #45
117	2	2006-04-04 11:04:55+00	2006-04-04 11:05:04+00	34	87	\N	\N	\N
118	2	2006-04-04 11:35:50+00	2006-04-04 11:35:54+00	51	30	\N	\N	\N
119	2	2006-04-04 11:35:51+00	2006-04-04 11:35:54+00	7	30	\N	\N	\N
120	2	2006-04-04 11:36:15+00	2006-04-04 11:36:21+00	17	40	\N	\N	\N
121	2	2006-04-04 11:36:39+00	2006-04-04 11:36:47+00	6	90	\N	\N	\N
122	2	2006-04-04 11:37:06+00	2006-04-04 11:37:09+00	4	30	\N	\N	\N
123	2	2006-04-04 11:37:45+00	2006-04-04 11:37:49+00	48	40	\N	\N	\N
124	2	2006-04-04 11:38:07+00	2006-04-04 11:38:11+00	48	40	\N	\N	\N
125	2	2006-04-04 11:38:27+00	2006-04-04 11:38:32+00	41	10	\N	\N	\N
126	2	2006-04-04 11:38:48+00	2006-04-04 11:38:53+00	43	5	\N	\N	\N
127	2	2006-04-04 11:39:12+00	2006-04-04 11:39:29+00	40	40	\N	\N	\N
128	2	2006-04-04 11:39:50+00	2006-04-04 11:39:53+00	8	20	\N	\N	\N
129	2	2006-04-04 11:40:13+00	2006-04-04 11:40:16+00	80	15	\N	\N	\N
130	2	2006-04-04 11:40:32+00	2006-04-04 11:40:38+00	74	20	\N	\N	\N
131	2	2006-04-04 11:41:39+00	2006-04-04 11:41:45+00	72	40	\N	\N	\N
132	2	2006-04-04 11:42:17+00	2006-04-04 11:42:26+00	3	50	\N	\N	\N
133	2	2006-04-04 11:42:24+00	2006-04-04 11:42:26+00	8	3	\N	\N	\N
134	2	2006-04-04 11:42:48+00	2006-04-04 11:43:08+00	20	40	\N	\N	\N
135	2	2006-04-04 11:43:05+00	2006-04-04 11:43:08+00	52	40	\N	\N	\N
136	3	2006-04-25 17:04:05+00	2006-04-25 17:04:57+00	56	110	\N	\N	\N
\.


--
-- Data for Name: invoices; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.invoices (id, order_id, invoice_date, due_date, tax, shipping, amount_due) FROM stdin;
5	31	2006-03-22 16:08:59+00	\N	0.0000	0.0000	0.0000
6	32	2006-03-22 16:10:27+00	\N	0.0000	0.0000	0.0000
7	40	2006-03-24 10:41:41+00	\N	0.0000	0.0000	0.0000
8	39	2006-03-24 10:55:46+00	\N	0.0000	0.0000	0.0000
9	38	2006-03-24 10:56:57+00	\N	0.0000	0.0000	0.0000
10	37	2006-03-24 10:57:38+00	\N	0.0000	0.0000	0.0000
11	36	2006-03-24 10:58:40+00	\N	0.0000	0.0000	0.0000
12	35	2006-03-24 10:59:41+00	\N	0.0000	0.0000	0.0000
13	34	2006-03-24 11:00:55+00	\N	0.0000	0.0000	0.0000
14	33	2006-03-24 11:02:02+00	\N	0.0000	0.0000	0.0000
15	30	2006-03-24 11:03:00+00	\N	0.0000	0.0000	0.0000
16	56	2006-04-03 13:50:15+00	\N	0.0000	0.0000	0.0000
17	55	2006-04-04 11:05:04+00	\N	0.0000	0.0000	0.0000
18	51	2006-04-04 11:06:13+00	\N	0.0000	0.0000	0.0000
19	50	2006-04-04 11:06:56+00	\N	0.0000	0.0000	0.0000
20	48	2006-04-04 11:07:37+00	\N	0.0000	0.0000	0.0000
21	47	2006-04-04 11:08:14+00	\N	0.0000	0.0000	0.0000
22	46	2006-04-04 11:08:49+00	\N	0.0000	0.0000	0.0000
23	45	2006-04-04 11:09:24+00	\N	0.0000	0.0000	0.0000
24	79	2006-04-04 11:35:54+00	\N	0.0000	0.0000	0.0000
25	78	2006-04-04 11:36:21+00	\N	0.0000	0.0000	0.0000
26	77	2006-04-04 11:36:47+00	\N	0.0000	0.0000	0.0000
27	76	2006-04-04 11:37:09+00	\N	0.0000	0.0000	0.0000
28	75	2006-04-04 11:37:49+00	\N	0.0000	0.0000	0.0000
29	74	2006-04-04 11:38:11+00	\N	0.0000	0.0000	0.0000
30	73	2006-04-04 11:38:32+00	\N	0.0000	0.0000	0.0000
31	72	2006-04-04 11:38:53+00	\N	0.0000	0.0000	0.0000
32	71	2006-04-04 11:39:29+00	\N	0.0000	0.0000	0.0000
33	70	2006-04-04 11:39:53+00	\N	0.0000	0.0000	0.0000
34	69	2006-04-04 11:40:16+00	\N	0.0000	0.0000	0.0000
35	67	2006-04-04 11:40:38+00	\N	0.0000	0.0000	0.0000
36	42	2006-04-04 11:41:14+00	\N	0.0000	0.0000	0.0000
37	60	2006-04-04 11:41:45+00	\N	0.0000	0.0000	0.0000
38	63	2006-04-04 11:42:26+00	\N	0.0000	0.0000	0.0000
39	58	2006-04-04 11:43:08+00	\N	0.0000	0.0000	0.0000
\.


--
-- Data for Name: order_details; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.order_details (id, order_id, product_id, quantity, unit_price, discount, status_id, date_allocated, purchase_order_id, inventory_id) FROM stdin;
27	30	34	100.0000	14.0000	0	2	\N	96	83
28	30	80	30.0000	3.5000	0	2	\N	\N	63
29	31	7	10.0000	30.0000	0	2	\N	\N	64
30	31	51	10.0000	53.0000	0	2	\N	\N	65
31	31	80	10.0000	3.5000	0	2	\N	\N	66
32	32	1	15.0000	18.0000	0	2	\N	\N	67
33	32	43	20.0000	46.0000	0	2	\N	\N	68
34	33	19	30.0000	9.2000	0	2	\N	97	81
35	34	19	20.0000	9.2000	0	2	\N	\N	69
36	35	48	10.0000	12.7500	0	2	\N	\N	70
37	36	41	200.0000	9.6500	0	2	\N	98	79
38	37	8	17.0000	40.0000	0	2	\N	\N	71
39	38	43	300.0000	46.0000	0	2	\N	99	77
40	39	48	100.0000	12.7500	0	2	\N	100	75
41	40	81	200.0000	2.9900	0	2	\N	101	73
42	41	43	300.0000	46.0000	0	1	\N	102	104
43	42	6	10.0000	25.0000	0	2	\N	\N	84
44	42	4	10.0000	22.0000	0	2	\N	\N	85
45	42	19	10.0000	9.2000	0	2	\N	103	110
46	43	80	20.0000	3.5000	0	1	\N	\N	86
47	43	81	50.0000	2.9900	0	1	\N	\N	87
48	44	1	25.0000	18.0000	0	1	\N	\N	88
49	44	43	25.0000	46.0000	0	1	\N	\N	89
50	44	81	25.0000	2.9900	0	1	\N	\N	90
51	45	41	50.0000	9.6500	0	2	\N	104	116
52	45	40	50.0000	18.4000	0	2	\N	\N	91
53	46	57	100.0000	19.5000	0	2	\N	105	101
54	46	72	50.0000	34.8000	0	2	\N	106	114
55	47	34	300.0000	14.0000	0	2	\N	107	108
56	48	8	25.0000	40.0000	0	2	\N	108	106
57	48	19	25.0000	9.2000	0	2	\N	109	112
59	50	21	20.0000	10.0000	0	2	\N	\N	92
60	51	5	25.0000	21.3500	0	2	\N	\N	93
61	51	41	30.0000	9.6500	0	2	\N	\N	94
62	51	40	30.0000	18.4000	0	2	\N	\N	95
66	56	48	10.0000	12.7500	0	2	\N	111	99
67	55	34	87.0000	14.0000	0	2	\N	\N	117
68	79	7	30.0000	30.0000	0	2	\N	\N	119
69	79	51	30.0000	53.0000	0	2	\N	\N	118
70	78	17	40.0000	39.0000	0	2	\N	\N	120
71	77	6	90.0000	25.0000	0	2	\N	\N	121
72	76	4	30.0000	22.0000	0	2	\N	\N	122
73	75	48	40.0000	12.7500	0	2	\N	\N	123
74	74	48	40.0000	12.7500	0	2	\N	\N	124
75	73	41	10.0000	9.6500	0	2	\N	\N	125
76	72	43	5.0000	46.0000	0	2	\N	\N	126
77	71	40	40.0000	18.4000	0	2	\N	\N	127
78	70	8	20.0000	40.0000	0	2	\N	\N	128
79	69	80	15.0000	3.5000	0	2	\N	\N	129
80	67	74	20.0000	10.0000	0	2	\N	\N	130
81	60	72	40.0000	34.8000	0	2	\N	\N	131
82	63	3	50.0000	10.0000	0	2	\N	\N	132
83	63	8	3.0000	40.0000	0	2	\N	\N	133
84	58	20	40.0000	81.0000	0	2	\N	\N	134
85	58	52	40.0000	7.0000	0	2	\N	\N	135
86	80	56	10.0000	38.0000	0	1	\N	\N	136
90	81	81	0.0000	2.9900	0	5	\N	\N	\N
91	81	56	0.0000	38.0000	0	0	\N	\N	\N
\.


--
-- Data for Name: order_details_status; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.order_details_status (id, status_name) FROM stdin;
0	None
1	Allocated
2	Invoiced
3	Shipped
4	On Order
5	No Stock
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.orders (id, employee_id, customer_id, order_date, shipped_date, shipper_id, ship_name, ship_address, ship_city, ship_state_province, ship_zip_postal_code, ship_country_region, shipping_fee, taxes, payment_type, paid_date, notes, tax_rate, tax_status_id, status_id) FROM stdin;
30	9	27	2006-01-15 00:00:00+00	2006-01-22 00:00:00+00	2	Judye Balkwill	0 Stephen Crossing	Fort Wayne	IN	46852	USA	200.0000	0.0000	Check	2006-01-15 00:00:00+00	\N	0	\N	3
31	3	4	2006-01-20 00:00:00+00	2006-01-22 00:00:00+00	1	Marnie Teece	9152 Lake View Trail	Springfield	MO	65805	USA	5.0000	0.0000	Credit Card	2006-01-20 00:00:00+00	\N	0	\N	3
32	4	12	2006-01-22 00:00:00+00	2006-01-22 00:00:00+00	2	Concordia Clohisey	4686 Harbort Court	Fresno	CA	93740	USA	5.0000	0.0000	Credit Card	2006-01-22 00:00:00+00	\N	0	\N	3
33	6	8	2006-01-30 00:00:00+00	2006-01-31 00:00:00+00	3	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	50.0000	0.0000	Credit Card	2006-01-30 00:00:00+00	\N	0	\N	3
34	9	4	2006-02-06 00:00:00+00	2006-02-07 00:00:00+00	3	Marnie Teece	9152 Lake View Trail	Springfield	MO	65805	USA	4.0000	0.0000	Check	2006-02-06 00:00:00+00	\N	0	\N	3
35	3	29	2006-02-10 00:00:00+00	2006-02-12 00:00:00+00	2	Belita Coulthard	49 Mayfield Pass	Indianapolis	IN	46295	USA	7.0000	0.0000	Check	2006-02-10 00:00:00+00	\N	0	\N	3
36	4	3	2006-02-23 00:00:00+00	2006-02-25 00:00:00+00	2	Trixie Bellelli	17 Quincy Plaza	New Orleans	LA	70183	USA	7.0000	0.0000	Cash	2006-02-23 00:00:00+00	\N	0	\N	3
37	8	6	2006-03-06 00:00:00+00	2006-03-09 00:00:00+00	2	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	12.0000	0.0000	Credit Card	2006-03-06 00:00:00+00	\N	0	\N	3
38	9	28	2006-03-10 00:00:00+00	2006-03-11 00:00:00+00	3	Sumner Mussington	336 Arapahoe Way	Sacramento	CA	94250	USA	10.0000	0.0000	Check	2006-03-10 00:00:00+00	\N	0	\N	3
39	3	8	2006-03-22 00:00:00+00	2006-03-24 00:00:00+00	3	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	5.0000	0.0000	Check	2006-03-22 00:00:00+00	\N	0	\N	3
40	4	10	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	Tessie Talks	29 Texas Road	Jacksonville	FL	32230	USA	9.0000	0.0000	Credit Card	2006-03-24 00:00:00+00	\N	0	\N	3
41	1	7	2006-03-24 00:00:00+00	\N	\N	Joni Dyott	7258 Heffernan Parkway	Indianapolis	IN	46247	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
42	1	10	2006-03-24 00:00:00+00	2006-04-07 00:00:00+00	1	Tessie Talks	29 Texas Road	Jacksonville	FL	32230	USA	0.0000	0.0000	\N	\N	\N	0	\N	2
43	1	11	2006-03-24 00:00:00+00	\N	3	Rhody Fuzzens	7 Steensland Street	Myrtle Beach	SC	29579	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
44	1	1	2006-03-24 00:00:00+00	\N	\N	Marga Gladhill	63202 Aberg Hill	Duluth	MN	55811	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
45	1	28	2006-04-07 00:00:00+00	2006-04-07 00:00:00+00	3	Sumner Mussington	336 Arapahoe Way	Sacramento	CA	94250	USA	40.0000	0.0000	Credit Card	2006-04-07 00:00:00+00	\N	0	\N	3
46	7	9	2006-04-05 00:00:00+00	2006-04-05 00:00:00+00	1	Mendel Trew	442 Lukken Circle	Dayton	OH	45403	USA	100.0000	0.0000	Check	2006-04-05 00:00:00+00	\N	0	\N	3
47	6	6	2006-04-08 00:00:00+00	2006-04-08 00:00:00+00	2	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	300.0000	0.0000	Credit Card	2006-04-08 00:00:00+00	\N	0	\N	3
48	4	8	2006-04-05 00:00:00+00	2006-04-05 00:00:00+00	2	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	50.0000	0.0000	Check	2006-04-05 00:00:00+00	\N	0	\N	3
50	9	25	2006-04-05 00:00:00+00	2006-04-05 00:00:00+00	1	Alford Murden	59546 Wayridge Street	Sioux Falls	SD	57105	USA	5.0000	0.0000	Cash	2006-04-05 00:00:00+00	\N	0	\N	3
51	9	26	2006-04-05 00:00:00+00	2006-04-05 00:00:00+00	3	Agata Basnett	1842 Mesta Lane	Louisville	KY	40210	USA	60.0000	0.0000	Credit Card	2006-04-05 00:00:00+00	\N	0	\N	3
55	1	29	2006-04-05 00:00:00+00	2006-04-05 00:00:00+00	2	Belita Coulthard	49 Mayfield Pass	Indianapolis	IN	46295	USA	200.0000	0.0000	Check	2006-04-05 00:00:00+00	\N	0	\N	3
56	2	6	2006-04-03 00:00:00+00	2006-04-03 00:00:00+00	3	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	0.0000	0.0000	Check	2006-04-03 00:00:00+00	\N	0	\N	3
57	9	27	2006-04-22 00:00:00+00	2006-04-22 00:00:00+00	2	Judye Balkwill	0 Stephen Crossing	Fort Wayne	IN	46852	USA	200.0000	0.0000	Check	2006-04-22 00:00:00+00	\N	0	\N	0
58	3	4	2006-04-22 00:00:00+00	2006-04-22 00:00:00+00	1	Marnie Teece	9152 Lake View Trail	Springfield	MO	65805	USA	5.0000	0.0000	Credit Card	2006-04-22 00:00:00+00	\N	0	\N	3
59	4	12	2006-04-22 00:00:00+00	2006-04-22 00:00:00+00	2	Concordia Clohisey	4686 Harbort Court	Fresno	CA	93740	USA	5.0000	0.0000	Credit Card	2006-04-22 00:00:00+00	\N	0	\N	0
60	6	8	2006-04-30 00:00:00+00	2006-04-30 00:00:00+00	3	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	50.0000	0.0000	Credit Card	2006-04-30 00:00:00+00	\N	0	\N	3
61	9	4	2006-04-07 00:00:00+00	2006-04-07 00:00:00+00	3	Marnie Teece	9152 Lake View Trail	Springfield	MO	65805	USA	4.0000	0.0000	Check	2006-04-07 00:00:00+00	\N	0	\N	0
62	3	29	2006-04-12 00:00:00+00	2006-04-12 00:00:00+00	2	Belita Coulthard	49 Mayfield Pass	Indianapolis	IN	46295	USA	7.0000	0.0000	Check	2006-04-12 00:00:00+00	\N	0	\N	0
63	4	3	2006-04-25 00:00:00+00	2006-04-25 00:00:00+00	2	Trixie Bellelli	17 Quincy Plaza	New Orleans	LA	70183	USA	7.0000	0.0000	Cash	2006-04-25 00:00:00+00	\N	0	\N	3
64	8	6	2006-05-09 00:00:00+00	2006-05-09 00:00:00+00	2	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	12.0000	0.0000	Credit Card	2006-05-09 00:00:00+00	\N	0	\N	0
65	9	28	2006-05-11 00:00:00+00	2006-05-11 00:00:00+00	3	Sumner Mussington	336 Arapahoe Way	Sacramento	CA	94250	USA	10.0000	0.0000	Check	2006-05-11 00:00:00+00	\N	0	\N	0
66	3	8	2006-05-24 00:00:00+00	2006-05-24 00:00:00+00	3	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	5.0000	0.0000	Check	2006-05-24 00:00:00+00	\N	0	\N	0
67	4	10	2006-05-24 00:00:00+00	2006-05-24 00:00:00+00	2	Tessie Talks	29 Texas Road	Jacksonville	FL	32230	USA	9.0000	0.0000	Credit Card	2006-05-24 00:00:00+00	\N	0	\N	3
68	1	7	2006-05-24 00:00:00+00	\N	\N	Joni Dyott	7258 Heffernan Parkway	Indianapolis	IN	46247	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
69	1	10	2006-05-24 00:00:00+00	\N	1	Tessie Talks	29 Texas Road	Jacksonville	FL	32230	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
70	1	11	2006-05-24 00:00:00+00	\N	3	Rhody Fuzzens	7 Steensland Street	Myrtle Beach	SC	29579	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
71	1	1	2006-05-24 00:00:00+00	\N	3	Marga Gladhill	63202 Aberg Hill	Duluth	MN	55811	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
72	1	28	2006-06-07 00:00:00+00	2006-06-07 00:00:00+00	3	Sumner Mussington	336 Arapahoe Way	Sacramento	CA	94250	USA	40.0000	0.0000	Credit Card	2006-06-07 00:00:00+00	\N	0	\N	3
73	7	9	2006-06-05 00:00:00+00	2006-06-05 00:00:00+00	1	Mendel Trew	442 Lukken Circle	Dayton	OH	45403	USA	100.0000	0.0000	Check	2006-06-05 00:00:00+00	\N	0	\N	3
74	6	6	2006-06-08 00:00:00+00	2006-06-08 00:00:00+00	2	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	300.0000	0.0000	Credit Card	2006-06-08 00:00:00+00	\N	0	\N	3
75	4	8	2006-06-05 00:00:00+00	2006-06-05 00:00:00+00	2	Lorianne O'Lynn	2 Commercial Road	Oklahoma City	OK	73152	USA	50.0000	0.0000	Check	2006-06-05 00:00:00+00	\N	0	\N	3
76	9	25	2006-06-05 00:00:00+00	2006-06-05 00:00:00+00	1	Alford Murden	59546 Wayridge Street	Sioux Falls	SD	57105	USA	5.0000	0.0000	Cash	2006-06-05 00:00:00+00	\N	0	\N	3
77	9	26	2006-06-05 00:00:00+00	2006-06-05 00:00:00+00	3	Agata Basnett	1842 Mesta Lane	Louisville	KY	40210	USA	60.0000	0.0000	Credit Card	2006-06-05 00:00:00+00	\N	0	\N	3
78	1	29	2006-06-05 00:00:00+00	2006-06-05 00:00:00+00	2	Belita Coulthard	49 Mayfield Pass	Indianapolis	IN	46295	USA	200.0000	0.0000	Check	2006-06-05 00:00:00+00	\N	0	\N	3
79	2	6	2006-06-23 00:00:00+00	2006-06-23 00:00:00+00	3	Emma Squirrel	86 Ridgeway Crossing	Largo	FL	34643	USA	0.0000	0.0000	Check	2006-06-23 00:00:00+00	\N	0	\N	3
80	2	4	2006-04-25 17:03:55+00	\N	\N	Marnie Teece	9152 Lake View Trail	Springfield	MO	65805	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
81	2	3	2006-04-25 17:26:53+00	\N	\N	Trixie Bellelli	17 Quincy Plaza	New Orleans	LA	70183	USA	0.0000	0.0000	\N	\N	\N	0	\N	0
\.


--
-- Data for Name: orders_status; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.orders_status (id, status_name) FROM stdin;
0	New
1	Invoiced
2	Shipped
3	Closed
\.


--
-- Data for Name: orders_tax_status; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.orders_tax_status (id, tax_status_name) FROM stdin;
0	Tax Exempt
1	Taxable
\.


--
-- Data for Name: privileges; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.privileges (id, privilege_name) FROM stdin;
2	Purchase Approvals
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.products (supplier_ids, id, product_code, product_name, description, standard_cost, list_price, reorder_level, target_level, quantity_per_unit, discontinued, minimum_reorder_quantity, category, attachments) FROM stdin;
4	1	NWTB-1	Northwind Traders Chai	\N	13.5000	18.0000	10	40	10 boxes x 20 bags	f	10	Beverages	\\x
10	3	NWTCO-3	Northwind Traders Syrup	\N	7.5000	10.0000	25	100	12 - 550 ml bottles	f	25	Condiments	\\x
10	4	NWTCO-4	Northwind Traders Cajun Seasoning	\N	16.5000	22.0000	10	40	48 - 6 oz jars	f	10	Condiments	\\x
10	5	NWTO-5	Northwind Traders Olive Oil	\N	16.0125	21.3500	10	40	36 boxes	f	10	Oil	\\x
2;6	6	NWTJP-6	Northwind Traders Boysenberry Spread	\N	18.7500	25.0000	25	100	12 - 8 oz jars	f	25	Jams, Preserves	\\x
2	7	NWTDFN-7	Northwind Traders Dried Pears	\N	22.5000	30.0000	10	40	12 - 1 lb pkgs.	f	10	Dried Fruit & Nuts	\\x
8	8	NWTS-8	Northwind Traders Curry Sauce	\N	30.0000	40.0000	10	40	12 - 12 oz jars	f	10	Sauces	\\x
2;6	14	NWTDFN-14	Northwind Traders Walnuts	\N	17.4375	23.2500	10	40	40 - 100 g pkgs.	f	10	Dried Fruit & Nuts	\\x
6	17	NWTCFV-17	Northwind Traders Fruit Cocktail	\N	29.2500	39.0000	10	40	15.25 OZ	f	10	Canned Fruit & Vegetables	\\x
1	19	NWTBGM-19	Northwind Traders Chocolate Biscuits Mix	\N	6.9000	9.2000	5	20	10 boxes x 12 pieces	f	5	Baked Goods & Mixes	\\x
2;6	20	NWTJP-6	Northwind Traders Marmalade	\N	60.7500	81.0000	10	40	30 gift boxes	f	10	Jams, Preserves	\\x
1	21	NWTBGM-21	Northwind Traders Scones	\N	7.5000	10.0000	5	20	24 pkgs. x 4 pieces	f	5	Baked Goods & Mixes	\\x
4	34	NWTB-34	Northwind Traders Beer	\N	10.5000	14.0000	15	60	24 - 12 oz bottles	f	15	Beverages	\\x
7	40	NWTCM-40	Northwind Traders Crab Meat	\N	13.8000	18.4000	30	120	24 - 4 oz tins	f	30	Canned Meat	\\x
6	41	NWTSO-41	Northwind Traders Clam Chowder	\N	7.2375	9.6500	10	40	12 - 12 oz cans	f	10	Soups	\\x
3;4	43	NWTB-43	Northwind Traders Coffee	\N	34.5000	46.0000	25	100	16 - 500 g tins	f	25	Beverages	\\x
10	48	NWTCA-48	Northwind Traders Chocolate	\N	9.5625	12.7500	25	100	10 pkgs	f	25	Candy	\\x
2	51	NWTDFN-51	Northwind Traders Dried Apples	\N	39.7500	53.0000	10	40	50 - 300 g pkgs.	f	10	Dried Fruit & Nuts	\\x
1	52	NWTG-52	Northwind Traders Long Grain Rice	\N	5.2500	7.0000	25	100	16 - 2 kg boxes	f	25	Grains	\\x
1	56	NWTP-56	Northwind Traders Gnocchi	\N	28.5000	38.0000	30	120	24 - 250 g pkgs.	f	30	Pasta	\\x
1	57	NWTP-57	Northwind Traders Ravioli	\N	14.6250	19.5000	20	80	24 - 250 g pkgs.	f	20	Pasta	\\x
8	65	NWTS-65	Northwind Traders Hot Pepper Sauce	\N	15.7875	21.0500	10	40	32 - 8 oz bottles	f	10	Sauces	\\x
8	66	NWTS-66	Northwind Traders Tomato Sauce	\N	12.7500	17.0000	20	80	24 - 8 oz jars	f	20	Sauces	\\x
5	72	NWTD-72	Northwind Traders Mozzarella	\N	26.1000	34.8000	10	40	24 - 200 g pkgs.	f	10	Dairy products	\\x
2;6	74	NWTDFN-74	Northwind Traders Almonds	\N	7.5000	10.0000	5	20	5 kg pkg.	f	5	Dried Fruit & Nuts	\\x
10	77	NWTCO-77	Northwind Traders Mustard	\N	9.7500	13.0000	15	60	12 boxes	f	15	Condiments	\\x
2	80	NWTDFN-80	Northwind Traders Dried Plums	\N	3.0000	3.5000	50	75	1 lb bag	f	25	Dried Fruit & Nuts	\\x
3	81	NWTB-81	Northwind Traders Green Tea	\N	2.0000	2.9900	100	125	20 bags per box	f	25	Beverages	\\x
1	82	NWTC-82	Northwind Traders Granola	\N	2.0000	4.0000	20	100	\N	f	\N	Cereal	\\x
9	83	NWTCS-83	Northwind Traders Potato Chips	\N	0.5000	1.8000	30	200	\N	f	\N	Chips, Snacks	\\x
1	85	NWTBGM-85	Northwind Traders Brownie Mix	\N	9.0000	12.4900	10	20	3 boxes	f	5	Baked Goods & Mixes	\\x
1	86	NWTBGM-86	Northwind Traders Cake Mix	\N	10.5000	15.9900	10	20	4 boxes	f	5	Baked Goods & Mixes	\\x
7	87	NWTB-87	Northwind Traders Tea	\N	2.0000	4.0000	20	50	100 count per box	f	\N	Beverages	\\x
6	88	NWTCFV-88	Northwind Traders Pears	\N	1.0000	1.3000	10	40	15.25 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	89	NWTCFV-89	Northwind Traders Peaches	\N	1.0000	1.5000	10	40	15.25 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	90	NWTCFV-90	Northwind Traders Pineapple	\N	1.0000	1.8000	10	40	15.25 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	91	NWTCFV-91	Northwind Traders Cherry Pie Filling	\N	1.0000	2.0000	10	40	15.25 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	92	NWTCFV-92	Northwind Traders Green Beans	\N	1.0000	1.2000	10	40	14.5 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	93	NWTCFV-93	Northwind Traders Corn	\N	1.0000	1.2000	10	40	14.5 OZ	f	\N	Canned Fruit & Vegetables	\\x
6	94	NWTCFV-94	Northwind Traders Peas	\N	1.0000	1.5000	10	40	14.5 OZ	f	\N	Canned Fruit & Vegetables	\\x
7	95	NWTCM-95	Northwind Traders Tuna Fish	\N	0.5000	2.0000	30	50	5 oz	f	\N	Canned Meat	\\x
7	96	NWTCM-96	Northwind Traders Smoked Salmon	\N	2.0000	4.0000	30	50	5 oz	f	\N	Canned Meat	\\x
1	97	NWTC-82	Northwind Traders Hot Cereal	\N	3.0000	5.0000	50	200	\N	f	\N	Cereal	\\x
6	98	NWTSO-98	Northwind Traders Vegetable Soup	\N	1.0000	1.8900	100	200	\N	f	\N	Soups	\\x
6	99	NWTSO-99	Northwind Traders Chicken Soup	\N	1.0000	1.9500	100	200	\N	f	\N	Soups	\\x
\.


--
-- Data for Name: purchase_order_details; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.purchase_order_details (id, purchase_order_id, product_id, quantity, unit_cost, date_received, posted_to_inventory, inventory_id) FROM stdin;
238	90	1	40.0000	14.0000	2006-01-22 00:00:00+00	t	59
239	91	3	100.0000	8.0000	2006-01-22 00:00:00+00	t	54
240	91	4	40.0000	16.0000	2006-01-22 00:00:00+00	t	55
241	91	5	40.0000	16.0000	2006-01-22 00:00:00+00	t	56
242	92	6	100.0000	19.0000	2006-01-22 00:00:00+00	t	40
243	92	7	40.0000	22.0000	2006-01-22 00:00:00+00	t	41
244	92	8	40.0000	30.0000	2006-01-22 00:00:00+00	t	42
245	92	14	40.0000	17.0000	2006-01-22 00:00:00+00	t	43
246	92	17	40.0000	29.0000	2006-01-22 00:00:00+00	t	44
247	92	19	20.0000	7.0000	2006-01-22 00:00:00+00	t	45
248	92	20	40.0000	61.0000	2006-01-22 00:00:00+00	t	46
249	92	21	20.0000	8.0000	2006-01-22 00:00:00+00	t	47
250	90	34	60.0000	10.0000	2006-01-22 00:00:00+00	t	60
251	92	40	120.0000	14.0000	2006-01-22 00:00:00+00	t	48
252	92	41	40.0000	7.0000	2006-01-22 00:00:00+00	t	49
253	90	43	100.0000	34.0000	2006-01-22 00:00:00+00	t	61
254	92	48	100.0000	10.0000	2006-01-22 00:00:00+00	t	50
255	92	51	40.0000	40.0000	2006-01-22 00:00:00+00	t	51
256	93	52	100.0000	5.0000	2006-01-22 00:00:00+00	t	37
257	93	56	120.0000	28.0000	2006-01-22 00:00:00+00	t	38
258	93	57	80.0000	15.0000	2006-01-22 00:00:00+00	t	39
259	91	65	40.0000	16.0000	2006-01-22 00:00:00+00	t	57
260	91	66	80.0000	13.0000	2006-01-22 00:00:00+00	t	58
261	94	72	40.0000	26.0000	2006-01-22 00:00:00+00	t	36
262	92	74	20.0000	8.0000	2006-01-22 00:00:00+00	t	52
263	92	77	60.0000	10.0000	2006-01-22 00:00:00+00	t	53
264	95	80	75.0000	3.0000	2006-01-22 00:00:00+00	t	35
265	90	81	125.0000	2.0000	2006-01-22 00:00:00+00	t	62
266	96	34	100.0000	10.0000	2006-01-22 00:00:00+00	t	82
267	97	19	30.0000	7.0000	2006-01-22 00:00:00+00	t	80
268	98	41	200.0000	7.0000	2006-01-22 00:00:00+00	t	78
269	99	43	300.0000	34.0000	2006-01-22 00:00:00+00	t	76
270	100	48	100.0000	10.0000	2006-01-22 00:00:00+00	t	74
271	101	81	200.0000	2.0000	2006-01-22 00:00:00+00	t	72
272	102	43	300.0000	34.0000	\N	f	\N
273	103	19	10.0000	7.0000	2006-04-17 00:00:00+00	t	111
274	104	41	50.0000	7.0000	2006-04-06 00:00:00+00	t	115
275	105	57	100.0000	15.0000	2006-04-05 00:00:00+00	t	100
276	106	72	50.0000	26.0000	2006-04-05 00:00:00+00	t	113
277	107	34	300.0000	10.0000	2006-04-05 00:00:00+00	t	107
278	108	8	25.0000	30.0000	2006-04-05 00:00:00+00	t	105
279	109	19	25.0000	7.0000	2006-04-05 00:00:00+00	t	109
280	110	43	250.0000	34.0000	2006-04-10 00:00:00+00	t	103
281	90	1	40.0000	14.0000	\N	f	\N
282	92	19	20.0000	7.0000	\N	f	\N
283	111	34	50.0000	10.0000	2006-04-04 00:00:00+00	t	102
285	91	3	50.0000	8.0000	\N	f	\N
286	91	4	40.0000	16.0000	\N	f	\N
288	140	85	10.0000	9.0000	\N	f	\N
289	141	6	10.0000	18.7500	\N	f	\N
290	142	1	1.0000	13.5000	\N	f	\N
292	146	20	40.0000	60.0000	\N	f	\N
293	146	51	40.0000	39.0000	\N	f	\N
294	147	40	120.0000	13.0000	\N	f	\N
295	148	72	40.0000	26.0000	\N	f	\N
\.


--
-- Data for Name: purchase_order_status; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.purchase_order_status (id, status) FROM stdin;
0	New
1	Submitted
2	Approved
3	Closed
\.


--
-- Data for Name: purchase_orders; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.purchase_orders (id, supplier_id, created_by, submitted_date, creation_date, status_id, expected_date, shipping_fee, taxes, payment_date, payment_amount, payment_method, notes, approved_by, approved_date, submitted_by) FROM stdin;
90	1	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
91	3	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
92	2	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
93	5	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
94	6	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
95	4	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-01-22 00:00:00+00	2
96	1	5	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #30	2	2006-01-22 00:00:00+00	5
97	2	7	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #33	2	2006-01-22 00:00:00+00	7
98	2	4	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #36	2	2006-01-22 00:00:00+00	4
99	1	3	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #38	2	2006-01-22 00:00:00+00	3
100	2	9	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #39	2	2006-01-22 00:00:00+00	9
101	1	2	2006-01-14 00:00:00+00	2006-01-22 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #40	2	2006-01-22 00:00:00+00	2
102	1	1	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #41	2	2006-04-04 00:00:00+00	1
103	2	1	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #42	2	2006-04-04 00:00:00+00	1
104	2	1	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #45	2	2006-04-04 00:00:00+00	1
105	5	7	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	Check	Purchase generated based on Order #46	2	2006-04-04 00:00:00+00	7
106	6	7	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #46	2	2006-04-04 00:00:00+00	7
107	1	6	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #47	2	2006-04-04 00:00:00+00	6
108	2	4	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #48	2	2006-04-04 00:00:00+00	4
109	2	4	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #48	2	2006-04-04 00:00:00+00	4
110	1	3	2006-03-24 00:00:00+00	2006-03-24 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #49	2	2006-04-04 00:00:00+00	3
111	1	2	2006-03-31 00:00:00+00	2006-03-31 00:00:00+00	2	\N	0.0000	0.0000	\N	0.0000	\N	Purchase generated based on Order #56	2	2006-04-04 00:00:00+00	2
140	6	\N	2006-04-25 00:00:00+00	2006-04-25 16:40:51+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-04-25 16:41:33+00	2
141	8	\N	2006-04-25 00:00:00+00	2006-04-25 17:10:35+00	2	\N	0.0000	0.0000	\N	0.0000	\N	\N	2	2006-04-25 17:10:55+00	2
142	8	\N	2006-04-25 00:00:00+00	2006-04-25 17:18:29+00	2	\N	0.0000	0.0000	\N	0.0000	Check	\N	2	2006-04-25 17:18:51+00	2
146	2	2	2006-04-26 18:26:37+00	2006-04-26 18:26:37+00	1	\N	0.0000	0.0000	\N	0.0000	\N	\N	\N	\N	2
147	7	2	2006-04-26 18:33:28+00	2006-04-26 18:33:28+00	1	\N	0.0000	0.0000	\N	0.0000	\N	\N	\N	\N	2
148	5	2	2006-04-26 18:33:52+00	2006-04-26 18:33:52+00	1	\N	0.0000	0.0000	\N	0.0000	\N	\N	\N	\N	2
\.


--
-- Data for Name: sales_reports; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.sales_reports (group_by, display, title, filter_row_source, "default") FROM stdin;
Category	Category	Sales By Category	SELECT DISTINCT [Category] FROM [products] ORDER BY [Category];	f
country_region	Country/Region	Sales By Country	SELECT DISTINCT [country_region] FROM [customers Extended] ORDER BY [country_region];	f
Customer ID	Customer	Sales By Customer	SELECT DISTINCT [Company] FROM [customers Extended] ORDER BY [Company];	f
employee_id	Employee	Sales By Employee	SELECT DISTINCT [Employee Name] FROM [employees Extended] ORDER BY [Employee Name];	f
Product ID	Product	Sales by Product	SELECT DISTINCT [Product Name] FROM [products] ORDER BY [Product Name];	t
\.


--
-- Data for Name: shippers; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.shippers (id, company, last_name, first_name, email_address, job_title, business_phone, home_phone, mobile_phone, fax_number, address, city, state_province, zip_postal_code, country_region, web_page, notes, attachments) FROM stdin;
1	Shipping Company A	\N	\N	\N	\N	\N	\N	\N	\N	123 Any Street	Memphis	TN	99999	USA	\N	\N	\\x
2	Shipping Company B	\N	\N	\N	\N	\N	\N	\N	\N	123 Any Street	Memphis	TN	99999	USA	\N	\N	\\x
3	Shipping Company C	\N	\N	\N	\N	\N	\N	\N	\N	123 Any Street	Memphis	TN	99999	USA	\N	\N	\\x
\.


--
-- Data for Name: strings; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.strings (string_id, string_data) FROM stdin;
2	Northwind Traders
3	Cannot remove posted inventory!
4	Back ordered product filled for Order #|
5	Discounted price below cost!
6	Insufficient inventory.
7	Insufficient inventory. Do you want to create a purchase order?
8	Purchase orders were successfully created for | products
9	There are no products below their respective reorder levels
10	Must specify customer name!
11	Restocking will generate purchase orders for all products below desired inventory levels.  Do you want to continue?
12	Cannot create purchase order.  No suppliers listed for specified product
13	Discounted price is below cost!
14	Do you want to continue?
15	Order is already invoiced. Do you want to print the invoice?
16	Order does not contain any line items
17	Cannot create invoice!  Inventory has not been allocated for each specified product.
18	Sorry, there are no sales in the specified time period
19	Product successfully restocked.
21	Product does not need restocking! Product is already at desired inventory level.
22	Product restocking failed!
23	Invalid login specified!
24	Must first select reported!
25	Changing supplier will remove purchase line items, continue?
26	Purchase orders were successfully submitted for | products.  Do you want to view the restocking report?
27	There was an error attempting to restock inventory levels.
28	| product(s) were successfully restocked.  Do you want to view the restocking report?
29	You cannot remove purchase line items already posted to inventory!
30	There was an error removing one or more purchase line items.
31	You cannot modify quantity for purchased product already received or posted to inventory.
32	You cannot modify price for purchased product already received or posted to inventory.
33	Product has been successfully posted to inventory.
34	Sorry, product cannot be successfully posted to inventory.
35	There are orders with this product on back order.  Would you like to fill them now?
36	Cannot post product to inventory without specifying received date!
37	Do you want to post received product to inventory?
38	Initialize purchase, orders, and inventory data?
39	Must first specify employee name!
40	Specified user must be logged in to approve purchase!
41	Purchase order must contain completed line items before it can be approved
42	Sorry, you do not have permission to approve purchases.
43	Purchase successfully approved
44	Purchase cannot be approved
45	Purchase successfully submitted for approval
46	Purchase cannot be submitted for approval
47	Sorry, purchase order does not contain line items
48	Do you want to cancel this order?
49	Canceling an order will permanently delete the order.  Are you sure you want to cancel?
100	Your order was successfully canceled.
101	Cannot cancel an order that has items received and posted to inventory.
102	There was an error trying to cancel this order.
103	The invoice for this order has not yet been created.
104	Shipping information is not complete.  Please specify all shipping information and try again.
105	Cannot mark as shipped.  Order must first be invoiced!
106	Cannot cancel an order that has already shipped!
107	Must first specify salesperson!
108	Order is now marked closed.
109	Order must first be marked shipped before closing.
110	Must first specify payment information!
111	There was an error attempting to restock inventory levels.  | product(s) were successfully restocked.
112	You must supply a Unit Cost.
113	Fill back ordered product, Order #|
114	Purchase generated based on Order #|
\.


--
-- Data for Name: suppliers; Type: TABLE DATA; Schema: northwind; Owner: -
--

COPY northwind.suppliers (id, company, last_name, first_name, email_address, job_title, business_phone, home_phone, mobile_phone, fax_number, address, city, state_province, zip_postal_code, country_region, web_page, notes, attachments) FROM stdin;
1	Supplier A	Andersen	Elizabeth A.	\N	Sales Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
2	Supplier B	Weiler	Cornelia	\N	Sales Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
3	Supplier C	Kelley	Madeleine	\N	Sales Representative	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
4	Supplier D	Sato	Naoki	\N	Marketing Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
5	Supplier E	Hernandez-Echevarria	Amaya	\N	Sales Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
6	Supplier F	Hayakawa	Satomi	\N	Marketing Assistant	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
7	Supplier G	Glasson	Stuart	\N	Marketing Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
8	Supplier H	Dunton	Bryn Paul	\N	Sales Representative	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
9	Supplier I	Sandberg	Mikael	\N	Sales Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
10	Supplier J	Sousa	Luis	\N	Sales Manager	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\\x
\.


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.customers_id_seq', 29, true);


--
-- Name: employees_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.employees_id_seq', 9, true);


--
-- Name: inventory_transactions_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.inventory_transactions_id_seq', 136, true);


--
-- Name: invoices_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.invoices_id_seq', 39, true);


--
-- Name: order_details_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.order_details_id_seq', 91, true);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.orders_id_seq', 81, true);


--
-- Name: privileges_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.privileges_id_seq', 2, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.products_id_seq', 99, true);


--
-- Name: purchase_order_details_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.purchase_order_details_id_seq', 295, true);


--
-- Name: purchase_orders_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.purchase_orders_id_seq', 148, true);


--
-- Name: shippers_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.shippers_id_seq', 3, true);


--
-- Name: strings_string_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.strings_string_id_seq', 114, true);


--
-- Name: suppliers_id_seq; Type: SEQUENCE SET; Schema: northwind; Owner: -
--

SELECT pg_catalog.setval('northwind.suppliers_id_seq', 10, true);


--
-- Name: customers idx_17514_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.customers
    ADD CONSTRAINT idx_17514_primary PRIMARY KEY (id);


--
-- Name: employees idx_17536_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.employees
    ADD CONSTRAINT idx_17536_primary PRIMARY KEY (id);


--
-- Name: employee_privileges idx_17556_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.employee_privileges
    ADD CONSTRAINT idx_17556_primary PRIMARY KEY (employee_id, privilege_id);


--
-- Name: inventory_transactions idx_17561_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions
    ADD CONSTRAINT idx_17561_primary PRIMARY KEY (id);


--
-- Name: inventory_transaction_types idx_17566_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transaction_types
    ADD CONSTRAINT idx_17566_primary PRIMARY KEY (id);


--
-- Name: invoices idx_17571_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.invoices
    ADD CONSTRAINT idx_17571_primary PRIMARY KEY (id);


--
-- Name: orders idx_17580_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT idx_17580_primary PRIMARY KEY (id);


--
-- Name: orders_status idx_17597_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders_status
    ADD CONSTRAINT idx_17597_primary PRIMARY KEY (id);


--
-- Name: orders_tax_status idx_17600_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders_tax_status
    ADD CONSTRAINT idx_17600_primary PRIMARY KEY (id);


--
-- Name: order_details idx_17605_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details
    ADD CONSTRAINT idx_17605_primary PRIMARY KEY (id);


--
-- Name: order_details_status idx_17612_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details_status
    ADD CONSTRAINT idx_17612_primary PRIMARY KEY (id);


--
-- Name: privileges idx_17617_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.privileges
    ADD CONSTRAINT idx_17617_primary PRIMARY KEY (id);


--
-- Name: products idx_17624_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.products
    ADD CONSTRAINT idx_17624_primary PRIMARY KEY (id);


--
-- Name: purchase_orders idx_17640_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_orders
    ADD CONSTRAINT idx_17640_primary PRIMARY KEY (id);


--
-- Name: purchase_order_details idx_17654_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_details
    ADD CONSTRAINT idx_17654_primary PRIMARY KEY (id);


--
-- Name: purchase_order_status idx_17659_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_status
    ADD CONSTRAINT idx_17659_primary PRIMARY KEY (id);


--
-- Name: sales_reports idx_17663_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.sales_reports
    ADD CONSTRAINT idx_17663_primary PRIMARY KEY (group_by);


--
-- Name: shippers idx_17674_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.shippers
    ADD CONSTRAINT idx_17674_primary PRIMARY KEY (id);


--
-- Name: strings idx_17696_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.strings
    ADD CONSTRAINT idx_17696_primary PRIMARY KEY (string_id);


--
-- Name: suppliers idx_17703_primary; Type: CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.suppliers
    ADD CONSTRAINT idx_17703_primary PRIMARY KEY (id);


--
-- Name: idx_17514_city; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_city ON northwind.customers USING btree (city);


--
-- Name: idx_17514_company; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_company ON northwind.customers USING btree (company);


--
-- Name: idx_17514_first_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_first_name ON northwind.customers USING btree (first_name);


--
-- Name: idx_17514_last_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_last_name ON northwind.customers USING btree (last_name);


--
-- Name: idx_17514_state_province; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_state_province ON northwind.customers USING btree (state_province);


--
-- Name: idx_17514_zip_postal_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17514_zip_postal_code ON northwind.customers USING btree (zip_postal_code);


--
-- Name: idx_17536_city; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_city ON northwind.employees USING btree (city);


--
-- Name: idx_17536_company; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_company ON northwind.employees USING btree (company);


--
-- Name: idx_17536_first_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_first_name ON northwind.employees USING btree (first_name);


--
-- Name: idx_17536_last_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_last_name ON northwind.employees USING btree (last_name);


--
-- Name: idx_17536_state_province; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_state_province ON northwind.employees USING btree (state_province);


--
-- Name: idx_17536_zip_postal_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17536_zip_postal_code ON northwind.employees USING btree (zip_postal_code);


--
-- Name: idx_17556_employee_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17556_employee_id ON northwind.employee_privileges USING btree (employee_id);


--
-- Name: idx_17556_privilege_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17556_privilege_id ON northwind.employee_privileges USING btree (privilege_id);


--
-- Name: idx_17561_customer_order_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17561_customer_order_id ON northwind.inventory_transactions USING btree (customer_order_id);


--
-- Name: idx_17561_product_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17561_product_id ON northwind.inventory_transactions USING btree (product_id);


--
-- Name: idx_17561_purchase_order_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17561_purchase_order_id ON northwind.inventory_transactions USING btree (purchase_order_id);


--
-- Name: idx_17561_transaction_type; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17561_transaction_type ON northwind.inventory_transactions USING btree (transaction_type);


--
-- Name: idx_17571_fk_invoices_orders1_idx; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17571_fk_invoices_orders1_idx ON northwind.invoices USING btree (order_id);


--
-- Name: idx_17571_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17571_id ON northwind.invoices USING btree (id);


--
-- Name: idx_17580_customer_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_customer_id ON northwind.orders USING btree (customer_id);


--
-- Name: idx_17580_employee_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_employee_id ON northwind.orders USING btree (employee_id);


--
-- Name: idx_17580_fk_orders_orders_status1; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_fk_orders_orders_status1 ON northwind.orders USING btree (status_id);


--
-- Name: idx_17580_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_id ON northwind.orders USING btree (id);


--
-- Name: idx_17580_ship_zip_postal_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_ship_zip_postal_code ON northwind.orders USING btree (ship_zip_postal_code);


--
-- Name: idx_17580_shipper_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_shipper_id ON northwind.orders USING btree (shipper_id);


--
-- Name: idx_17580_tax_status; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17580_tax_status ON northwind.orders USING btree (tax_status_id);


--
-- Name: idx_17605_fk_order_details_order_details_status1_idx; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_fk_order_details_order_details_status1_idx ON northwind.order_details USING btree (status_id);


--
-- Name: idx_17605_fk_order_details_orders1_idx; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_fk_order_details_orders1_idx ON northwind.order_details USING btree (order_id);


--
-- Name: idx_17605_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_id ON northwind.order_details USING btree (id);


--
-- Name: idx_17605_inventory_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_inventory_id ON northwind.order_details USING btree (inventory_id);


--
-- Name: idx_17605_product_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_product_id ON northwind.order_details USING btree (product_id);


--
-- Name: idx_17605_purchase_order_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17605_purchase_order_id ON northwind.order_details USING btree (purchase_order_id);


--
-- Name: idx_17624_product_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17624_product_code ON northwind.products USING btree (product_code);


--
-- Name: idx_17640_created_by; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17640_created_by ON northwind.purchase_orders USING btree (created_by);


--
-- Name: idx_17640_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE UNIQUE INDEX idx_17640_id ON northwind.purchase_orders USING btree (id);


--
-- Name: idx_17640_id_2; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17640_id_2 ON northwind.purchase_orders USING btree (id);


--
-- Name: idx_17640_status_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17640_status_id ON northwind.purchase_orders USING btree (status_id);


--
-- Name: idx_17640_supplier_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17640_supplier_id ON northwind.purchase_orders USING btree (supplier_id);


--
-- Name: idx_17654_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17654_id ON northwind.purchase_order_details USING btree (id);


--
-- Name: idx_17654_inventory_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17654_inventory_id ON northwind.purchase_order_details USING btree (inventory_id);


--
-- Name: idx_17654_product_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17654_product_id ON northwind.purchase_order_details USING btree (product_id);


--
-- Name: idx_17654_purchase_order_id; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17654_purchase_order_id ON northwind.purchase_order_details USING btree (purchase_order_id);


--
-- Name: idx_17674_city; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_city ON northwind.shippers USING btree (city);


--
-- Name: idx_17674_company; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_company ON northwind.shippers USING btree (company);


--
-- Name: idx_17674_first_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_first_name ON northwind.shippers USING btree (first_name);


--
-- Name: idx_17674_last_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_last_name ON northwind.shippers USING btree (last_name);


--
-- Name: idx_17674_state_province; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_state_province ON northwind.shippers USING btree (state_province);


--
-- Name: idx_17674_zip_postal_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17674_zip_postal_code ON northwind.shippers USING btree (zip_postal_code);


--
-- Name: idx_17703_city; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_city ON northwind.suppliers USING btree (city);


--
-- Name: idx_17703_company; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_company ON northwind.suppliers USING btree (company);


--
-- Name: idx_17703_first_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_first_name ON northwind.suppliers USING btree (first_name);


--
-- Name: idx_17703_last_name; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_last_name ON northwind.suppliers USING btree (last_name);


--
-- Name: idx_17703_state_province; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_state_province ON northwind.suppliers USING btree (state_province);


--
-- Name: idx_17703_zip_postal_code; Type: INDEX; Schema: northwind; Owner: -
--

CREATE INDEX idx_17703_zip_postal_code ON northwind.suppliers USING btree (zip_postal_code);


--
-- Name: employee_privileges fk_employee_privileges_employees1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.employee_privileges
    ADD CONSTRAINT fk_employee_privileges_employees1 FOREIGN KEY (employee_id) REFERENCES northwind.employees(id);


--
-- Name: employee_privileges fk_employee_privileges_privileges1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.employee_privileges
    ADD CONSTRAINT fk_employee_privileges_privileges1 FOREIGN KEY (privilege_id) REFERENCES northwind.privileges(id);


--
-- Name: inventory_transactions fk_inventory_transactions_inventory_transaction_types1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions
    ADD CONSTRAINT fk_inventory_transactions_inventory_transaction_types1 FOREIGN KEY (transaction_type) REFERENCES northwind.inventory_transaction_types(id);


--
-- Name: inventory_transactions fk_inventory_transactions_orders1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions
    ADD CONSTRAINT fk_inventory_transactions_orders1 FOREIGN KEY (customer_order_id) REFERENCES northwind.orders(id);


--
-- Name: inventory_transactions fk_inventory_transactions_products1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions
    ADD CONSTRAINT fk_inventory_transactions_products1 FOREIGN KEY (product_id) REFERENCES northwind.products(id);


--
-- Name: inventory_transactions fk_inventory_transactions_purchase_orders1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.inventory_transactions
    ADD CONSTRAINT fk_inventory_transactions_purchase_orders1 FOREIGN KEY (purchase_order_id) REFERENCES northwind.purchase_orders(id);


--
-- Name: invoices fk_invoices_orders1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.invoices
    ADD CONSTRAINT fk_invoices_orders1 FOREIGN KEY (order_id) REFERENCES northwind.orders(id);


--
-- Name: order_details fk_order_details_order_details_status1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details
    ADD CONSTRAINT fk_order_details_order_details_status1 FOREIGN KEY (status_id) REFERENCES northwind.order_details_status(id);


--
-- Name: order_details fk_order_details_orders1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details
    ADD CONSTRAINT fk_order_details_orders1 FOREIGN KEY (order_id) REFERENCES northwind.orders(id);


--
-- Name: order_details fk_order_details_products1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.order_details
    ADD CONSTRAINT fk_order_details_products1 FOREIGN KEY (product_id) REFERENCES northwind.products(id);


--
-- Name: orders fk_orders_customers; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES northwind.customers(id);


--
-- Name: orders fk_orders_employees1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT fk_orders_employees1 FOREIGN KEY (employee_id) REFERENCES northwind.employees(id);


--
-- Name: orders fk_orders_orders_status1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT fk_orders_orders_status1 FOREIGN KEY (status_id) REFERENCES northwind.orders_status(id);


--
-- Name: orders fk_orders_orders_tax_status1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT fk_orders_orders_tax_status1 FOREIGN KEY (tax_status_id) REFERENCES northwind.orders_tax_status(id);


--
-- Name: orders fk_orders_shippers1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.orders
    ADD CONSTRAINT fk_orders_shippers1 FOREIGN KEY (shipper_id) REFERENCES northwind.shippers(id);


--
-- Name: purchase_order_details fk_purchase_order_details_inventory_transactions1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_details
    ADD CONSTRAINT fk_purchase_order_details_inventory_transactions1 FOREIGN KEY (inventory_id) REFERENCES northwind.inventory_transactions(id);


--
-- Name: purchase_order_details fk_purchase_order_details_products1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_details
    ADD CONSTRAINT fk_purchase_order_details_products1 FOREIGN KEY (product_id) REFERENCES northwind.products(id);


--
-- Name: purchase_order_details fk_purchase_order_details_purchase_orders1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_order_details
    ADD CONSTRAINT fk_purchase_order_details_purchase_orders1 FOREIGN KEY (purchase_order_id) REFERENCES northwind.purchase_orders(id);


--
-- Name: purchase_orders fk_purchase_orders_employees1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_orders
    ADD CONSTRAINT fk_purchase_orders_employees1 FOREIGN KEY (created_by) REFERENCES northwind.employees(id);


--
-- Name: purchase_orders fk_purchase_orders_purchase_order_status1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_orders
    ADD CONSTRAINT fk_purchase_orders_purchase_order_status1 FOREIGN KEY (status_id) REFERENCES northwind.purchase_order_status(id);


--
-- Name: purchase_orders fk_purchase_orders_suppliers1; Type: FK CONSTRAINT; Schema: northwind; Owner: -
--

ALTER TABLE ONLY northwind.purchase_orders
    ADD CONSTRAINT fk_purchase_orders_suppliers1 FOREIGN KEY (supplier_id) REFERENCES northwind.suppliers(id);


--
-- PostgreSQL database dump complete
--

