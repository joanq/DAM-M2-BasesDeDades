= MongoDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat consultes 3

Utilitzarem la col·lecció link:https://github.com/mledoze/countries/blob/master/countries.json[countries].

Per importar la base de dades segueix els passos següents:

1. Descarrega el fitxer. Importa'l amb:
+
----
$ mongoimport -d test -c countries --jsonArray <ruta al fitxer countries.json>
----

2. El sistema t'ha d'informar que s'han importat 248 documents.

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- La consulta demanada.
- Els resultats obtinguts (o part dels resultats si en retorna molts).
====

1. Cerca en quants països s'utilitza l'euro.
+
[source,js]
----
> db.countries.find({currency:"EUR"}).count()
----

2. Cerca en quines subregions està dividida Europa.
+
[source,js]
----
> db.countries.distinct("subregion",{region:"Europe"})
----

3. Cerca quin país té el domini d'Internet (_tld_) `.tk`.
+
[source,js]
----
> db.countries.find({tld:".tk"},{"name.common":1,_id:0})
----

4. Cerca quins països tenen més d'un domini d'Internet.
+
[source,js]
----
> db.countries.find({"tld.1":{$exists:true}},{"name.common":1,tld:1,_id:0})
----

5. Cerca quins països tenen una jota al seu nom (majúscula o minúscula).
+
[source,js]
----
> db.countries.find({"name.common":/[jJ]/},{"name.common":1,_id:0})
----

6. Cerca el nom nadiu del país que en espanyol es diu "Japón".
+
[source,js]
----
> db.countries.find({"translations.spa.common":"Japón"},{"name.native":1,_id:0})
----

7. Cerca el nom dels països que tenen més de 5 països veïns.
+
[source,js]
----
> db.countries.find({"borders.5":{$exists:true}},{borders:1,"name.common":1,_id:0})
----

8. Cerca en quants països es parla francès.
+
[source,js]
----
> db.countries.find({"languages.fra":{$exists:true}}).count()
46
----

9. Cerca el nom comú i el nom oficial dels països africans en què es parla
francès.
+
[source,js]
----
> db.countries.find(
  {
    "languages.fra":{$exists:true},
    "region":"Africa"
  }, {_id:0,"name.common":1,"name.official":1}
)
{ "name" : { "common" : "Burundi", "official" : "Republic of Burundi" } }
{ "name" : { "common" : "Benin", "official" : "Republic of Benin" } }
{ "name" : { "common" : "Burkina Faso", "official" : "Burkina Faso" } }
{ "name" : { "common" : "Central African Republic", "official" : "Central African Republic" } }
{ "name" : { "common" : "Ivory Coast", "official" : "Republic of Côte d'Ivoire" } }
{ "name" : { "common" : "Cameroon", "official" : "Republic of Cameroon" } }
{ "name" : { "common" : "DR Congo", "official" : "Democratic Republic of the Congo" } }
{ "name" : { "common" : "Republic of the Congo", "official" : "Republic of the Congo" } }
----

10. Cerca el nom comú i el continent (_region_) dels països no africans en què es parla àrab.
+
[source,js]
----
> db.countries.find({"languages.ara":{$exists:true},region:{$ne:"Africa"}}, {region:1,"name.common":1,_id:0})
{ "name" : { "common" : "United Arab Emirates" }, "region" : "Asia" }
{ "name" : { "common" : "Bahrain" }, "region" : "Asia" }
{ "name" : { "common" : "Iraq" }, "region" : "Asia" }
{ "name" : { "common" : "Israel" }, "region" : "Asia" }
{ "name" : { "common" : "Jordan" }, "region" : "Asia" }
{ "name" : { "common" : "Kuwait" }, "region" : "Asia" }
{ "name" : { "common" : "Lebanon" }, "region" : "Asia" }
{ "name" : { "common" : "Oman" }, "region" : "Asia" }
{ "name" : { "common" : "Palestine" }, "region" : "Asia" }
{ "name" : { "common" : "Qatar" }, "region" : "Asia" }
{ "name" : { "common" : "Saudi Arabia" }, "region" : "Asia" }
{ "name" : { "common" : "Syria" }, "region" : "Asia" }
{ "name" : { "common" : "Yemen" }, "region" : "Asia" }
----

11. Cerca com es diu Espanya en rus.
+
[source,js]
----
> db.countries.find(
  {"name.common":"Spain"},
  {"translations.rus.common":1,_id:0}
)
{ "translations" : { "rus" : { "common" : "Испания" } } }
----

12. Cerca en quants països existeix més d'una divisa oficial.
+
[source,js]
----
> db.countries.find(
  {"currency.1":{$exists:1}},
  {currency:1,"name.common":1,_id:0}
).count()
13
----

13. Cerca el nom dels països que són limítrofs amb Sèrbia.
+
[source,js]
----
> db.countries.find(
  {"cca3":{$in:
    db.countries.find(
      {"name.common":"Serbia"},
      {borders:1,_id:0}
    ).toArray()[0].borders
  }},
  {"name.common":1,_id:0}
)

> db.countries.find(
  {"cca3":{$in:
    db.countries.findOne(
      {"name.common":"Serbia"},
      {borders:1,_id:0}
    )["borders"]
  }},
  {"name.common":1,_id:0}
)

> db.countries.find(
  {borders:
    db.countries.findOne({"name.common":"Serbia"}, {cca3:1,_id:0}).cca3
  },{"name.common":1,_id:0}
)
----

14. Cerca el nom dels països en què l'anglès és l'única llengua oficial.
+
[source,js]
----
> db.countries.find(
  {
    "languages.eng":{$exists:true},
    $where:"Object.keys(this.languages).length==1"
  },
  {"name.common":1,_id:0}
)
----

15. Cerca quants idiomes es parlen a Espanya.
+
[source,js]
----
> Object.keys(
  db.countries.find(
    {"name.common":"Spain"},
    {"languages":1,_id:0}
  ).toArray()[0].languages
).length

> Object.keys(
  db.countries.findOne(
    {"name.common":"Spain"},
    {"languages":1,_id:0}
  )["languages"]
).length
5
----
