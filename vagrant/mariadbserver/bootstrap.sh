#!/bin/sh

# Instal·lació mariadb
## Per un bug a la versió oficial de Debian, instal·lem des de la pàgina oficial
apt-get update
apt-get install -y apt-transport-https curl
mkdir -p /etc/apt/keyrings
curl -o /etc/apt/keyrings/mariadb-keyring.pgp 'https://mariadb.org/mariadb_release_signing_key.pgp'
cp /vagrant/mariadb.sources /etc/apt/sources.list.d
apt-get update
apt-get install -y mariadb-server

# Importa BD
mariadb < /db/sakila_onlytables/sakila-schema.sql
mariadb < /db/sakila_onlytables/sakila-data.sql
mariadb < /db/miniwind.sql
mariadb < /db/Chinook_MySql_AutoIncrementPKs.sql
mariadb < /db/Chinook_MySql.sql
mariadb < /db/lastnames.sql
mariadb < /db/KnightsDB.sql
mariadb < /db/traders.sql
mariadb < /db/northwind.sql
mariadb < /db/northwind-data.sql
mariadb < /db/hotel.sql
mariadb < /db/ies23g.sql

# Configuració de MariaDB:
# - Permet connexions des de qualsevol host
# - Activa GROUP BY estricte
# - Permet || com a CONCAT (PIPES_AS_CONCAT)
# - No permet " com a delimitador de cadenes, només ' (ANSI_QUOTES)
cp /vagrant/50-server.cnf /etc/mysql/mariadb.conf.d

# Crea l'usuari admin amb accés remot
mariadb << EOF
CREATE OR REPLACE USER admin@'%' IDENTIFIED BY 'super3';
GRANT ALL ON *.* TO admin@'%';
EOF
systemctl restart mariadb

