--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE institut;
--
-- Name: institut; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE institut WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Catalan_Spain.1252' LC_CTYPE = 'Catalan_Spain.1252';


ALTER DATABASE institut OWNER TO postgres;

\connect institut

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: escola; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA escola;


ALTER SCHEMA escola OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alumnes; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.alumnes (
    id integer NOT NULL,
    nom character varying(15),
    cognom1 character varying(15),
    cognom2 character varying(15),
    grup integer,
    actiu boolean DEFAULT true
);


ALTER TABLE escola.alumnes OWNER TO postgres;

--
-- Name: alumnes_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.alumnes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.alumnes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: qualificacions; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.qualificacions (
    id integer NOT NULL,
    idalumne integer,
    idunitatformativa integer,
    idprofessor integer,
    nota1c integer,
    nota2c integer
);


ALTER TABLE escola.qualificacions OWNER TO postgres;

--
-- Name: alumnesunitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.qualificacions ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.alumnesunitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: aules; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.aules (
    id integer NOT NULL,
    nomaula character varying(10),
    capacitat integer
);


ALTER TABLE escola.aules OWNER TO postgres;

--
-- Name: aules_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.aules ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.aules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cicles; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.cicles (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    descripcio character varying(50),
    nivell character(1) DEFAULT 'S'::bpchar
);


ALTER TABLE escola.cicles OWNER TO postgres;

--
-- Name: cicles_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.cicles ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.cicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grups; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.grups (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    tutor integer,
    cicle integer,
    curs integer
);


ALTER TABLE escola.grups OWNER TO postgres;

--
-- Name: grups_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.grups ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.grups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grupsunitatsformatives; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.grupsunitatsformatives (
    id integer NOT NULL,
    idgrup integer,
    idunitatformativa integer,
    idprofessor integer,
    idaula integer
);


ALTER TABLE escola.grupsunitatsformatives OWNER TO postgres;

--
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.grupsunitatsformatives ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.grupsunitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: moduls; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.moduls (
    modul integer NOT NULL,
    hores integer NOT NULL,
    nom character varying(40) NOT NULL,
    cicle integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE escola.moduls OWNER TO postgres;

--
-- Name: moduls_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.moduls ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.moduls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: professor; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.professor (
    id integer NOT NULL,
    nom character varying(20) NOT NULL,
    cognom character varying(20)
);


ALTER TABLE escola.professor OWNER TO postgres;

--
-- Name: professor_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.professor ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME escola.professor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: unitatsformatives; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.unitatsformatives (
    id integer NOT NULL,
    idmodul integer,
    unitat integer,
    hores integer
);


ALTER TABLE escola.unitatsformatives OWNER TO postgres;

--
-- Name: unitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.unitatsformatives ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.unitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: alumnes; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.alumnes (id, nom, cognom1, cognom2, grup, actiu) FROM stdin;
3	Raul	Godinez	Gonzalez	1	t
2	Daniel	Fraile	Moya	1	t
4	Didac	Llamas	Barranco	1	t
5	Ismael	Lopez	Pellicer	1	t
6	Adrian	Marin	Caballero	1	t
7	Javi	Mauri	Ullate	1	t
8	Victor	Melo	Carreto	1	t
9	Roger	Moreno	Luque	1	t
10	Jose Manuel	Munoz	Marin	1	t
1	Amjad	El Khayat	Talbi	1	t
11	Hassan	Naveed	Khan	1	t
12	Isaac	Noa	Cejuela	1	t
13	Sergi	Pellejero	Dishmey	1	t
14	Alex	Pladevall	Jimenez	1	t
15	Luis	Portillo	Ruiz	1	t
16	Eduard	Rodriguez	Diez	1	t
17	Yuric	Romero	Gargallo	1	t
18	Gorka	Manas	Perez	2	t
19	Alejandro	Morillo	Roldan	2	t
20	Arnau	Vilamana	Mateu	2	t
21	Emilio	Maso	Atroche	4	t
22	Raul	Perez	Arredondo	4	t
23	David	Hernandez	Aibar	3	t
\.


--
-- Data for Name: aules; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.aules (id, nomaula, capacitat) FROM stdin;
1	C2	30
2	C3	30
3	C4	30
4	F3	22
5	1.6	40
6	A1	20
\.


--
-- Data for Name: cicles; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.cicles (id, nom, descripcio, nivell) FROM stdin;
1	ASIX	Administració de Sistemes	S
2	DAM	Desenvolupament d'Aplicacions	S
3	DAMVI	Desenvolupament d'Aplicacions VideoJocs	S
4	SMX	Sistemes MicroInformàtics	M
\.


--
-- Data for Name: grups; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.grups (id, nom, tutor, cicle, curs) FROM stdin;
1	ASIX1A	1	1	1
2	ASXI2	2	1	2
3	DAMVI2	7	3	2
4	DAM2A	3	2	2
\.


--
-- Data for Name: grupsunitatsformatives; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.grupsunitatsformatives (id, idgrup, idunitatformativa, idprofessor, idaula) FROM stdin;
1	1	1	2	1
2	1	2	2	1
3	1	3	2	1
4	1	4	3	1
5	1	5	3	1
6	1	6	3	1
7	1	7	1	1
8	1	8	1	1
9	1	9	1	1
10	1	10	4	1
11	1	11	4	1
12	1	12	4	1
13	1	13	5	1
14	1	14	5	1
15	1	15	5	1
16	1	16	6	4
17	1	17	6	4
\.


--
-- Data for Name: moduls; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.moduls (modul, hores, nom, cicle, id) FROM stdin;
1	231	ImplantaciÃ³ Sistemes Operatius	1	1
2	165	GestiÃ³ de base de dades	1	2
3	165	ProgramaciÃ³ bÃ¡sica	1	3
4	99	Llenguatge de Marques	1	4
5	99	Fonaments de Maquinari	1	5
6	132	Admin de Sistemes Operatius	1	6
10	99	AdministraciÃ³ de SGBDs	1	7
9	66	ImplantaciÃ³ d'Aplicacions WEB	1	8
7	165	PlanificaciÃ³ i Admin Xarxes	1	9
8	99	Serveis de Xarxa i Internet	1	12
12	99	FormaciÃ³ i OrientaciÃ³ Laboral	1	10
11	99	Seguretat i Alta Disponibilitat	1	13
13	66	Empresa i Iniciativa Empresarial	1	14
14	99	Projecte	1	15
15	317	FormaciÃ³ en centres de treball	1	16
\.


--
-- Data for Name: professor; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.professor (id, nom, cognom) FROM stdin;
1	Gemma	Raimat
2	Ivan	Baranda
3	Gregorio	Santamaria
4	Isabel	de Andrés
5	Lino	de la Muñoza
6	Empar	Mestre
7	Marc	Albareda
\.


--
-- Data for Name: qualificacions; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.qualificacions (id, idalumne, idunitatformativa, idprofessor, nota1c, nota2c) FROM stdin;
1	2	4	3	6	\N
2	2	5	3	8	\N
3	1	5	3	6	\N
4	3	5	3	7	\N
5	4	4	3	4	\N
6	4	5	3	5	\N
7	5	4	3	5	\N
8	5	5	3	5	\N
9	6	4	3	9	\N
10	6	5	3	9	\N
11	7	4	3	5	\N
12	7	5	3	4	\N
13	8	5	3	7	\N
14	9	4	3	4	\N
15	9	5	3	7	\N
16	10	4	3	7	\N
17	10	5	3	5	\N
18	11	4	3	6	\N
19	11	5	3	6	\N
20	12	4	3	6	\N
21	12	5	3	6	\N
22	13	4	3	6	\N
23	13	5	3	8	\N
24	14	4	3	9	\N
25	14	5	3	8	\N
26	15	4	3	8	\N
27	15	5	3	9	\N
28	16	4	3	4	\N
29	16	5	3	6	\N
30	17	4	3	7	\N
31	17	5	3	6	\N
32	18	5	3	0	\N
33	19	5	3	4	\N
34	23	4	3	5	\N
35	23	5	3	6	\N
52	17	6	3	\N	\N
36	1	6	3	\N	\N
37	2	6	3	\N	\N
38	3	6	3	\N	\N
39	4	6	3	\N	\N
40	5	6	3	\N	\N
41	6	6	3	\N	\N
42	7	6	3	\N	\N
43	8	6	3	\N	\N
44	9	6	3	\N	\N
45	10	6	3	\N	\N
46	11	6	3	\N	\N
47	12	6	3	\N	\N
48	13	6	3	\N	\N
49	14	6	3	\N	\N
50	15	6	3	\N	\N
51	16	6	3	\N	\N
53	18	6	3	\N	\N
54	19	6	3	\N	\N
55	20	6	3	\N	\N
56	21	6	3	\N	\N
57	22	6	3	\N	\N
58	23	6	3	\N	\N
\.


--
-- Data for Name: unitatsformatives; Type: TABLE DATA; Schema: escola; Owner: postgres
--

COPY escola.unitatsformatives (id, idmodul, unitat, hores) FROM stdin;
10	4	1	45
11	4	2	27
12	4	3	27
13	5	1	44
1	1	1	72
2	1	2	90
3	1	3	36
4	2	1	44
5	2	2	88
7	3	1	85
8	3	2	50
9	3	3	30
16	9	1	44
17	9	2	44
6	7	1	66
14	10	1	66
15	10	2	33
18	1	4	33
19	5	2	33
20	6	1	99
21	6	2	33
22	12	1	25
23	12	2	25
24	12	3	25
25	12	4	24
26	5	3	33
27	9	3	66
28	13	1	24
29	13	2	24
30	13	3	27
31	13	4	24
32	8	1	33
33	8	2	33
34	2	3	33
35	7	3	33
36	14	1	66
37	15	1	99
38	16	1	317
\.


--
-- Name: alumnes_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.alumnes_id_seq', 23, true);


--
-- Name: alumnesunitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.alumnesunitatsformatives_id_seq', 58, true);


--
-- Name: aules_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.aules_id_seq', 6, true);


--
-- Name: cicles_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.cicles_id_seq', 4, true);


--
-- Name: grups_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.grups_id_seq', 4, true);


--
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.grupsunitatsformatives_id_seq', 17, true);


--
-- Name: moduls_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.moduls_id_seq', 30, true);


--
-- Name: professor_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.professor_id_seq', 1, true);


--
-- Name: unitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.unitatsformatives_id_seq', 17, true);


--
-- Name: alumnes alumnes_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.alumnes
    ADD CONSTRAINT alumnes_pk PRIMARY KEY (id);


--
-- Name: qualificacions alumnesunitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_pk PRIMARY KEY (id);


--
-- Name: aules aules_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.aules
    ADD CONSTRAINT aules_pk PRIMARY KEY (id);


--
-- Name: cicles cicles_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.cicles
    ADD CONSTRAINT cicles_pk PRIMARY KEY (id);


--
-- Name: grups grups_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_pk PRIMARY KEY (id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_pk PRIMARY KEY (id);


--
-- Name: moduls moduls_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_pk PRIMARY KEY (id);


--
-- Name: moduls moduls_un; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_un UNIQUE (cicle, modul);


--
-- Name: professor professor_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.professor
    ADD CONSTRAINT professor_pk PRIMARY KEY (id);


--
-- Name: unitatsformatives unitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.unitatsformatives
    ADD CONSTRAINT unitatsformatives_pk PRIMARY KEY (id);


--
-- Name: alumnes alumnes_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.alumnes
    ADD CONSTRAINT alumnes_fk FOREIGN KEY (grup) REFERENCES escola.grups(id);


--
-- Name: qualificacions alumnesunitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk FOREIGN KEY (idalumne) REFERENCES escola.alumnes(id);


--
-- Name: qualificacions alumnesunitatsformatives_fk2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk2 FOREIGN KEY (idunitatformativa) REFERENCES escola.unitatsformatives(id);


--
-- Name: qualificacions alumnesunitatsformatives_fk3; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk3 FOREIGN KEY (idprofessor) REFERENCES escola.professor(id);


--
-- Name: grups grups_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_fk FOREIGN KEY (cicle) REFERENCES escola.cicles(id);


--
-- Name: grups grups_fk2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_fk2 FOREIGN KEY (tutor) REFERENCES escola.professor(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk FOREIGN KEY (idaula) REFERENCES escola.aules(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_fk3; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk3 FOREIGN KEY (idprofessor) REFERENCES escola.professor(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_fk_1; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk_1 FOREIGN KEY (idunitatformativa) REFERENCES escola.unitatsformatives(id);


--
-- Name: grupsunitatsformatives grupsunitatsformatives_fk_2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk_2 FOREIGN KEY (idgrup) REFERENCES escola.grups(id);


--
-- Name: moduls moduls_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_fk FOREIGN KEY (cicle) REFERENCES escola.cicles(id);


--
-- Name: unitatsformatives unitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.unitatsformatives
    ADD CONSTRAINT unitatsformatives_fk FOREIGN KEY (idmodul) REFERENCES escola.moduls(id);


--
-- PostgreSQL database dump complete
--

